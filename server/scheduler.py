import json
import time
import schedule

from server.consts import LAKE_RELATIONS_PATH
from map_handler.update_measurements import update_measurements
from ModelFromNVE.icemodellingscripts.getIceThicknessLakes import update_data


def update_all_measurements(update_bbox: bool):
    """Loops through all lake names and calls get_measurements() on each lake

            Parameters:
                update_bbox (bool): Whether the bboxes should be updated or not.
    """

    try:
        # Parse all lake names to a list. Set encoding to utf-8 to retain scandinavian characters
        with open(LAKE_RELATIONS_PATH + 'all_lake_names.json', 'r', encoding='utf-8') as file:
            lake_names = json.load(file)

        for lake in lake_names:
            # NB dates are hard coded to january for demonstration.
            # For deployment, simply remove the date parameters.
            from_date = "2024-01-10"
            to_date = "2024-01-20"

            # Updates the data from the NVE model
            update_data(from_date, to_date, lake_name=lake,
                        sub_divs_folder=LAKE_RELATIONS_PATH + lake + '_centers.txt', update_all_bboxes=update_bbox)

            # Updates all the measurements for the current lake
            update_measurements(lake)
            print("\t", lake, " updated")

    except Exception as e:
        print(f"Failed in update_all_measurements(...): {e}")


class UpdateScheduler:
    def __init__(self):
        self.day_counter = 1

    def start(self):
        """Schedules the updating of all maps every three days"""
        try:
            print("Updating all lake data....")

            # Run update_all_measurements on startup
            update_all_measurements(True)

            # Schedule updates to occur daily
            schedule.every(1).days.do(self.daily_update)

            # Keep scheduler running indefinitely
            while True:
                schedule.run_pending()
                time.sleep(1)
        except Exception as e:
            print(f"Failed to schedule updates: {e}")

    def daily_update(self):
        if self.day_counter < 3:  # Day 1 or 2, no bbox update
            update_all_measurements(False)
            self.day_counter += 1  # Increment counter
        else:  # Day three, update all data including bbox
            update_all_measurements(True)
            self.day_counter = 1  # Reset counter


