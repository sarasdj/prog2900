import json
import os
from datetime import datetime
from server.data_processing.process_lidar_data import calculate_area_data, about_laz_file
from server.consts import LIDAR_DATA_PATH, LAKE_RELATIONS_PATH

def lidar_data_handler(self, cursor, sensorId, lake_name: str):
    # finds the thickness in all area in a lake
    status_code, thickness_data = input_new_Lidar_data(cursor, sensorId, lake_name)

    self.send_response(status_code)
    self.send_header("Content-type", "application/json")
    self.end_headers()

    # Write content data to response object
    self.wfile.write(json.dumps(thickness_data).encode('utf-8'))

# input_new_Lidar_data send new data gathered from the lidar and send it to the database (from the drone, most likely)
def input_new_Lidar_data(cursor, sensorId, lake_name: str) -> (int, str):
    """
    send the sensors data of the lake_name to database and update the jsonfile for specific body of water

    :param self (BaseHTTPRequestHandler): A instance of a BaseHTTPRequestHandler
    :param cursor (cursor): An Sqlite3 cursor object that points to the database
    :param sensorId: Id of the sensor used to measure the data
    :param lake_name (str): The name of the requested file/lake
    :return: (int, str): A tuple containing the status code and the meassurement data of the lidar files and where it
    was taken on the map
    """
    try:
        # read json data with path to data for specific water body
        file_path = os.path.join(LAKE_RELATIONS_PATH, lake_name + '_lidar_data.json')

        # check if water body data exist
        if not os.path.exists(file_path):
            return 404, f"no data for {lake_name} found"

        # get the areas from the map's data
        with open(file_path) as data:
            measurement_data = json.load(data)

        # check if map has been given areas
        if len(measurement_data) < 1:
            print("no area on map found")
            return 404, f"No areas in {lake_name} found"

        lidar_json_data = [] # container for the ice thickness data for all subdivs
        subdiv_json_data = [] # container for the ice thickness data for each area in "body of water"

        # get the ice thickness of each area on map
        for measurement in measurement_data:
            print(measurement)
            measurement_id = measurement["MeasurementID"]

            latitude = measurement["CenterLat"]
            longitude = measurement["CenterLon"]

            # path to each measurement data
            laz_file_path = os.path.join(LIDAR_DATA_PATH + lake_name + '/measurement_id_' + str(measurement_id) + '.laz')

            # data about the file read from
            about_laz = about_laz_file(laz_file_path)
            # scale factor of the ice thickness in meter
            scale_factor = max(about_laz[2])

            # time last updated data
            time_now = datetime.now().utcnow().replace(microsecond=0)

            # create a new measurement with the time the data is sent, sensor type, where
            # and an estimate of average thickness of ice on water body
            cursor.execute('''
                INSERT INTO Measurement(MeasurementID, SensorID, TimeMeasured, WaterBodyName, CenterLat, CenterLon) 
                VALUES (?,?,?,?,?,?);
            ''', (measurement_id, sensorId, time_now, lake_name, latitude, longitude))

            # Calculate each area's ice thickness based on the area coordinates given to the calculation model
            areas_data = calculate_area_data((latitude, longitude), lake_name, laz_file_path)

            if (areas_data):
                # store lidar data in json format
                # calculate data for each zone within the area
                for area in areas_data:
                    subId = int(area[0])        # id of area
                    map_lat, map_lng = area[1]  # lng and lat relative to map
                    ice_thickness = area[2]     # ice thickness in area

                    # find the average and minimum ice thickness in area
                    if (len(ice_thickness) != 0 or sum(ice_thickness) != 0):
                        average = sum(ice_thickness) / len(ice_thickness)
                        minimum_thickness = min(ice_thickness)
                    else:
                        average = 0
                        minimum_thickness = 0

                    # input the data into the database
                    cursor.execute('''
                    INSERT INTO SubdivisionMeasurementData(MeasurementID, TimeMeasured, SubdivID, WaterBodyName, 
                                                    MinimumThickness, AverageThickness, CalculatedSafety, Accuracy) 
                    VALUES (?,?,?,?,?,?,?,?);
                    ''', (measurement_id, time_now, subId, lake_name, float(minimum_thickness), float(average),
                          float(0.0), scale_factor))
                    sub_center = (map_lat, map_lng)

                    # set up json format of area's data
                    lidar_read = {
                        'SubdivID': subId,
                        'MinThickness': float(minimum_thickness),
                        'AvgThickness': float(average),
                        'CenLatitude': float(sub_center[0]),
                        'CenLongitude': float(sub_center[1]),
                        'Accuracy': scale_factor,
                    }

                    # add area to list of subdivisions
                    subdiv_json_data.append(lidar_read)
            else:
                print('No data found')

            # container for measurement data
            measurement_data = {
                'MeasurementID': measurement_id,
                'TimeMeasured': str(time_now),
                'CenterLat': latitude,
                'CenterLon': longitude,
                'Sensor': {
                    'SensorId': sensorId,
                    'SensorType': "LiDar",
                    "Active": True,
                },
                'Subdivisions': subdiv_json_data,
            }

            # add measurement data to list
            lidar_json_data.append(measurement_data)

        # send the changes to the database
        cursor.connection.commit()

        # container for content, might not be needed
        content = None

        # check if there are ice in area
        if len(lidar_json_data) > 0:
            if os.path.exists(file_path):
                os.remove(file_path)

            # convert list of lidar data to json
            content = json.dumps(lidar_json_data, indent=4)
            # write to file
            with open(file_path, "w") as file:
                file.write(content)
        else:
            # return empty list, since there were no ice in area scanned
            content = json.dumps([])
            print('No ice found in current area')

        # send response
        return 200, content

    # error handling
    except Exception as e:
        # rollback in case of error
        cursor.connection.rollback()
        return 500, f"An error occurred: {e} g".encode("utf-8")

