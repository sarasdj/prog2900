from server.consts import LIDAR_DATA_PATH
from server.data_processing.process_lidar_data import find_folder_files, calculate_area_data

def test_find_folder_files():
    path = './server/data_processing/unit_tests/test_files'
    actual = find_folder_files(path)
    expected = [path + '\\measurement_id_0.laz', path + '\\measurement_id_1.laz']
    assert actual == expected

def test_calculate_area_data_no_path():
    center = (1,2)
    lake_name = 'mjøsa'
    path = ''
    actual = calculate_area_data(center, lake_name, path)
    expected = []
    assert actual == expected

def test_calculate_area_data_no_name():
    center = (1,2)
    lake_name = ''
    path = './server/data_processing/unit_tests/test_files/measurement_id_0.laz'
    actual = calculate_area_data(center, lake_name, path)
    expected = []
    assert actual == expected

def test_calculate_area_data_no_data():
    center = (1000, 2000)
    lake_name = 'mjøsa'
    path = './server/data_processing/unit_tests/test_files/measurement_id_0.laz'
    actual = calculate_area_data(center, lake_name, path)
    expected = []
    print("res = ", actual)
    print("res = ", expected)
    assert actual == expected

def test_calculate_area_data():
    center = (61, 11)
    lake_name = 'mjøsa'
    path = './server/data_processing/unit_tests/test_files/measurement_id_0.laz'
    actual = calculate_area_data(center, lake_name, path)
    expected = []
    print("res = ", actual)
    assert actual is not None
