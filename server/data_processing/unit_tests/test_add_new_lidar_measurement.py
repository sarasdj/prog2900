import os

from server.data_processing.add_new_lidar_measurement import add_new_lidar_measurement
from server.consts import LAKE_RELATIONS_PATH, LIDAR_DATA_PATH

def test_add_new_lidar_measurement_invalid_lake() -> None:
    lake_name = 'test_lake'
    lat = 0.0
    lon = 0.0
    actual, _ = add_new_lidar_measurement(lake_name, lat, lon)
    expected = 404
    assert actual == expected

def test_add_new_lidar_measurement_valid_lake() -> None:
    # parameters for test
    lake_name = 'fake_for_test'
    lat = 0.0
    lon = 0.0

    # create temporary test file and directory
    file_path = os.path.join(LAKE_RELATIONS_PATH, f"{lake_name}_lidar_data.json")
    dir_path = os.path.join(LIDAR_DATA_PATH + lake_name)
    if not os.path.exists(file_path):
        with open(file_path, "w") as file:
            file.write("{}")
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    # expectations
    actual, _ = add_new_lidar_measurement(lake_name, lat, lon)
    expected = 200
    # remove temporary files and directories
    if os.path.exists(file_path):
        os.remove(file_path)
    if os.path.isdir(dir_path):
        os.rmdir(dir_path)

    # result
    assert actual == expected

