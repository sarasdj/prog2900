from unittest.mock import MagicMock
import pytest
from server.data_processing.input_new_data import input_new_Lidar_data

def test_new_input_invalid_cursor() -> None:
    # create a mock cursor
    mock_cursor = MagicMock()
    mock_cursor.execute.return_value = None
    sensor_id = 1
    test_lake_name = "test_lake"

    status_code, _ = input_new_Lidar_data(mock_cursor, sensor_id, test_lake_name)
    expected = 404
    assert status_code == expected

def test_new_input_invalid_lake() -> None:
    # create a mock cursor
    mock_cursor = MagicMock()
    mock_cursor.execute.side_effect = [
        ('Mjøsa'),
        (1, 'LiDar', 1),
        (1, 2, '2024-01-01 10:00:00', 'Mjøsa', 10.1234, 60.4567),
        (1, 1, '2024-01-01 10:00:00', 'Mjøsa', 3.2, 4.5, 2.1, 1.2),
    ]
    sensor_id = 1
    test_lake_name = "nolake"

    status_code, _ = input_new_Lidar_data(mock_cursor, sensor_id, test_lake_name)

    expected = 404
    assert status_code == expected

def test_new_input_unvalid_cursor() -> None:
    sensor_id = 1
    test_lake_name = "skumsjøen"
    # create a mock cursor
    mock_cursor = MagicMock()
    mock_cursor.execute.side_effect = \
        [
        ('Mjøsa'),
        (1, 'LiDar', 1),
        (1, 2, '2024-01-01 10:00:00', 'Mjøsa', 10.1234, 60.4567),
        (1, 1, '2024-01-01 10:00:00', 'Mjøsa', 3.2, 4.5, 2.1, 1.2),
    ]

    status_code, _ = input_new_Lidar_data(mock_cursor, sensor_id, test_lake_name)

    assert status_code == 500

def test_new_input_unvalid_cursor() -> None:
    sensor_id = 1
    test_lake_name = "Mjøsa"
    # create a mock cursor
    mock_cursor = MagicMock()
    mock_cursor.execute.side_effect = \
        [
        ('Mjøsa'),
        (1, 'LiDar', 1),
        (1, 2, '2024-01-01 10:00:00', 'Mjøsa', 10.1234, 60.4567),
        (1, 1, '2024-01-01 10:00:00', 'Mjøsa', 3.2, 4.5, 2.1, 1.2),
    ]

    status_code, _ = input_new_Lidar_data(mock_cursor, sensor_id, test_lake_name)

    assert status_code == 500
