from server.data_processing.area_processing import inArea, distance, closest_points, find_height, calculate_corners, define_gridareas, define_grid_lidardata

def test_inArea_inside() -> None:
    position = (2,2,2)
    range = [(0,0),(3,3)]
    actual = inArea(position, range)
    expected = True
    assert actual == expected

def test_inArea_outside() -> None:
    position = (5,2,5)
    range = [(0,0),(3,3)]
    actual = inArea(position, range)
    expected = False
    assert actual == expected

def test_inArea_inside_float() -> None:
    position = (3.1,3.5,2.2)
    range = [(0.4,0.4),(3.2,3.4)]
    actual = inArea(position, range)
    expected = False
    assert actual == expected

def test_distance_0() -> None:
    point1 = (0,0)
    point2 = (0,0)
    actual = distance(point1,point2)
    expected = 0
    assert actual == expected

def test_distance_5() -> None:
    point1 = (0,0)
    point2 = (4,3)
    actual = distance(point1,point2)
    expected = 5
    assert actual == expected

def test_closest_points_no_taken_points() -> None:
    point = (0,0)
    taken = []
    listOfPoints = [
        {
            "type": "Feature",
            "properties": {
                "sub_div_id": "0",
                "sub_div_center": [1, 1]
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            }
        }, {
            "type": "Feature",
            "properties": {
                "sub_div_id": "0",
                "sub_div_center": [1, 2]
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            }
        },
    ]
    actual = closest_points(point, listOfPoints, taken)
    expected = listOfPoints[0]
    assert actual == expected

def test_closest_points_one_taken_points() -> None:
    point = (0,0)
    listOfPoints = [
        {
            "type": "Feature",
            "properties": {
                "sub_div_id": "0",
                "sub_div_center": [1, 1]
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            }
        }, {
            "type": "Feature",
            "properties": {
                "sub_div_id": "0",
                "sub_div_center": [2, 3]
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            }
        },
    ]
    taken = [[1,1]]
    actual = closest_points(point, listOfPoints,taken)
    expected = listOfPoints[1]
    assert actual == expected

def test_closest_points_all_points_taken() -> None:
    point = (0,0)
    listOfPoints = [
        {
            "type": "Feature",
            "properties": {
                "sub_div_id": "0",
                "sub_div_center": [1, 1]
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            }
        }, {
            "type": "Feature",
            "properties": {
                "sub_div_id": "0",
                "sub_div_center": [2, 3]
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": []
            }
        },
    ]
    taken = [[1,1],[2,3]]
    actual = closest_points(point, listOfPoints,taken)
    expected = None
    assert actual == expected

def test_find_height() -> None:
    points = [(1, 2, 3), (1, 2, 4)]
    actual = find_height(points)
    expected = [1]
    assert actual == expected

def test_find_height_nothing() -> None:
    points = [(1, 2, 3), (1, 3, 4)]
    actual = find_height(points)
    expected = []
    assert actual == expected

def test_find_height_multiple() -> None:
    points = [(1, 3, 3), (1, 3, 4), (1, 3, 6), (2, 2, 5), (2, 2, 2)]
    actual = find_height(points)
    expected = [3, 3]
    assert actual == expected

def test_calculate_corners_correct_direction() -> None:
    x, y, offset = (10.0, 10.0, (10, 10))
    actual = calculate_corners(x, y, offset)

    actual = (
            actual[0][0] < x and actual[0][1] > y and # top left
            actual[1][0] > x and actual[1][1] > y and # top right
            actual[2][0] > x and actual[2][1] < y and # bottom right
            actual[3][0] < x and actual[3][1] < y     # bottom left
    )

    expected = True
    assert actual == expected

def test_define_gridareas_correct_nr_gridded() -> None:
    grid_size = 3
    actual = define_gridareas(60,10,(10,10), grid_size)
    expected = grid_size**2
    assert len(actual) == expected

def test_define_grid_lidardata_correct_nr_gridded() -> None:
    grid_size = 3
    actual = define_grid_lidardata(((0, 0), (60, 10)), grid_size, [(1,1,3),(1,1,6),(59,9,3),(59,9,9)])
    expected = [[3],[0],[0],[0],[0],[0],[0],[0],[6]]
    assert actual == expected

def test_define_grid_lidardata_no_points() -> None:
    grid_size = 3
    actual = define_grid_lidardata(((0, 0), (60, 10)), grid_size, [])
    expected = [[0],[0],[0],[0],[0],[0],[0],[0],[0]]
    assert actual == expected

