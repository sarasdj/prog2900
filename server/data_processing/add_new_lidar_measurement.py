import json
import os
from datetime import datetime

from server.consts import LIDAR_DATA_PATH, LAKE_RELATIONS_PATH

def add_new_lidar_measurement_handler(self, lake_name: str, lat: float, lon: float):
    status_code, updated_content = add_new_lidar_measurement(lake_name, lat, lon)

    self.send_response(status_code)
    self.send_header("Content-type", "application/json")
    self.end_headers()

    # Write content data to response object
    self.wfile.write(json.dumps(updated_content).encode('utf-8'))

def add_new_lidar_measurement(lake_name: str, lat: float, lon: float):
    """
    Adds a new lidar measurement in another location of a lake

    :param self (BaseHTTPRequestHandler): A instance of a BaseHTTPRequestHandler
    :param lake_name (str): The name of the requested file/lake
    :param lat (float): The latitude of the measurement
    :param lon (float): The longitude of the measurement
    :return (int, str): A tuple containing the status code and all lidar measurements in one body of water
    """
    try:
        # find out if lake exist in our database
        # sjekk hvordan sara sjekker dette - se i sqlDB eller mapper eller ...
        lake_data_path = os.path.join(LIDAR_DATA_PATH + lake_name)
        if not os.path.isdir(lake_data_path):
            return 404, f"lake does not exist: {lake_name}"

        # read json data with path to data for specific water body
        file_path = os.path.join(LAKE_RELATIONS_PATH, lake_name + '_lidar_data.json')

        # time last updated data
        time_now = datetime.now().utcnow().replace(microsecond=0)

        measurement_data = []
        content = []

        # check if water body data exist, else create
        if os.path.exists(file_path):
            # get the areas from the map's data
            with open(file_path) as data:
                measurement_data.append(json.load(data))

            # create an empty container for new measurement
            new_measurement_data = {
                'MeasurementID': len(measurement_data),
                'TimeMeasured': str(time_now),
                'CenterLat': lat,
                'CenterLon': lon,
                'Sensor': {
                    'SensorId': 2,
                    'SensorType': "LiDar",
                    "Active": True,
                },
                'Subdivisions': [],
            }

            if not measurement_data:
                measurement_data = new_measurement_data
            else:
                measurement_data.append(new_measurement_data)

            # convert data to json format
            content = json.dumps(measurement_data, indent=4)

        else:
            # create an empty container for new measurement
            new_measurement_data = {
                'MeasurementID': 0,
                'TimeMeasured': str(time_now),
                'CenterLat': lat,
                'CenterLon': lon,
                'Sensor': {
                    'SensorId': 2,
                    'SensorType': "LiDar",
                    "Active": True,
                },
                'Subdivisions': [],
            }
            content = json.dumps(new_measurement_data, indent=4)

        # remove existing file
        if os.path.exists(file_path):
            os.remove(file_path)

        # write to file
        with open(file_path, "w") as file:
            file.write(content)

        # send response
        return 200, content

    # error handling
    except Exception as e:
        return 500, f"An error occurred: {e}".encode("utf-8")


#print(add_new_lidar_measurement("mjøsa", 60.9, 10.9))