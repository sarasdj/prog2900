from server.Sentinelhub import sentinel_config, evalscript, box_funcitons
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv
import datetime as dt
import os



from sentinelhub import (
    CRS, BBox, DataCollection, SentinelHubRequest, SentinelHubStatistical, SHConfig,
    Geometry, bbox_to_dimensions, DownloadRequest, MimeType, MosaickingOrder, parse_time
)

# Helperfunction made b sentinelhub
def stats_to_df(stats_data):
    """Transform Statistical API response into a pandas.DataFrame"""
    df_data = []

    for single_data in stats_data["data"]:
        df_entry = {}
        is_valid_entry = True

        df_entry["interval_from"] = parse_time(single_data["interval"]["from"]).date()
        df_entry["interval_to"] = parse_time(single_data["interval"]["to"]).date()

        for output_name, output_data in single_data["outputs"].items():
            for band_name, band_values in output_data["bands"].items():
                band_stats = band_values["stats"]
                if band_stats["sampleCount"] == band_stats["noDataCount"]:
                    is_valid_entry = False
                    break

                for stat_name, value in band_stats.items():
                    col_name = f"{output_name}_{band_name}_{stat_name}"
                    if stat_name == "percentiles":
                        for perc, perc_val in value.items():
                            perc_col_name = f"{col_name}_{perc}"
                            df_entry[perc_col_name] = round(perc_val, 2)
                    elif isinstance(value, (int, float)):
                        if stat_name not in ["sampleCount", "noDataCount"]:
                            df_entry[col_name] = round(value, 2)
                        else:
                            df_entry[col_name] = value

        if is_valid_entry:
            df_data.append(df_entry)

    return pd.DataFrame(df_data)

def statistical_request_sentinel(config, evalscript, time_interval, maxcc, bbox):
    try:
        request = SentinelHubStatistical(
            aggregation=SentinelHubStatistical.aggregation(
                evalscript=evalscript,
                time_interval=time_interval,
                aggregation_interval="P1D"
            ),
            input_data=[
                SentinelHubStatistical.input_data(
                    data_collection=DataCollection.SENTINEL2_L1C,
                    maxcc=maxcc
                )
            ],
            bbox=bbox,
            config=config,
        )

        stats = request.get_data()

        dfs = [stats_to_df(polygon_stats) for polygon_stats in stats]

        return pd.concat(dfs)

    except Exception as e:
        print(f"Something is wrong with request, error: {e}")
        return None

def classify_ice(data):
    """ set a label in data based on st deviation"""

    ice_conditions = []

    # remove dates where the picture are cropped
    data = data[data['default_B0_noDataCount'] <= 1000]

    for st_dev in data['default_B0_stDev']:
        if st_dev > 35:
            ice_conditions.append('High probability of Cloud')
        elif st_dev > 6.5:
            ice_conditions.append('No ice') # does have the possability of being cracked ice aswell
        #elif st_dev > 9999:
        #    ice_conditions.append('Weak ice or cracked ice')
        else:
            ice_conditions.append('Ice')

    data['ice_condition'] = ice_conditions

    return data

def save_data_to_csv(file, data):
    filename = f"{file}.csv"
    data.to_csv(filename, index=False)

def append_data_to_csv(file, data):
    """ adds all data at to an alredy existing csv if the format is the same, if file does not exist, crate a new file
    using save_data_to_file(file, data) """
    try:
        temp = pd.read_csv(f"{file}.csv")
        temp = pd.concat([temp, data], ignore_index=True)
        temp.to_csv(f"{file}.csv", index=False)
    except FileNotFoundError:
        save_data_to_csv(file, data)
    except Exception as e:
        print(f"An error occured: {e}")
def get_last_date(data):
    """ Returns last date of the last picture in file, if file does not exist, returns previous August 1st. """
    if data is not None and not data.empty:  # Check if 'data' is not None and not empty
        df_date = pd.DataFrame(data, columns=['interval_to']).tail(1)
        if not df_date.empty:
            return df_date['interval_to'].iloc[0]  # Return the last date

    # If 'data' is None or empty, calculate the previous August 1st
    today = dt.datetime.now()
    prev_august = today.year if today.month >= 8 else today.year - 1
    prev_august_date = dt.datetime(prev_august, 8, 1)
    return prev_august_date.strftime("%Y-%m-%d")

def update_to_most_recent(file, BBox, evalscript = evalscript.lake_ice_index, maxcc = 0.4):
    """ if file exists, tries to get the most recent date. uses that date to do get all dates until now. Appends that data to the file
    if the file does not exist. create a new one and put all data into it.  """
    try:
        data = pd.read_csv(f"{file}.csv")
        last_date = get_last_date(data)
        today = dt.datetime.now().strftime("%Y-%m-%d")

        data = statistical_request_sentinel(config= sentinel_config.config, evalscript=evalscript,
                                            time_interval=(last_date, today),
                                            maxcc=maxcc, bbox=BBox)

        if data is not None and data.empty is False:
            data = classify_ice(data)
            append_data_to_csv(file, data)

        return

    except FileNotFoundError:
        last_date = get_last_date(None)
        today = dt.datetime.now().strftime("%Y-%m-%d")

        data = statistical_request_sentinel(config=sentinel_config.config, evalscript=evalscript,
                                       time_interval=(last_date, today),
                                       maxcc=maxcc, bbox=BBox)

        if data is not None:
            data = classify_ice(data)
            save_data_to_csv(file, data)
        return

def read_from_csv(file):
    """ Reads from name.csv and returns a data """
    try:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file = os.path.join(dir_path, file)
        data = pd.read_csv(f"{file}.csv")
        return data
    except FileNotFoundError:
        return None
    except Exception as e:
        print(f"An error occured: {e}")

def update_all_polynomials(file_name_format = "bbox_sat_data/lake"):
    """
    With all polynomials from box functions. uses getArealinfo to update the arealinfo for each area and creates
    or updates the square at id given index box.

    """

    all_bbox = box_funcitons.get_all_bbox()

    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_name_format = os.path.join(dir_path, file_name_format)
    for i, box in enumerate(all_bbox):
        update_to_most_recent(f"{file_name_format}_index_{i}", box)

    return

def get_ice_run_dates(data):
    """ Get the dates for when the ice is on and of"""
    """ Interpolates the date between two dates where the state for the ice_condition changes, "does count cloudy days " """
    ice_run_dates = []

    # Iterate through the DataFrame to find condition changes
    for i in range(len(data) - 1):
        current_condition = data.iloc[i]['ice_condition']
        next_condition = data.iloc[i + 1]['ice_condition']

        # Check if there is a change in condition, skipps clouds during fall, but assumes clouds to be breaking ice
        # if it is after ice as the cloud labeling has a probability to be caused by ice breakage.
        if current_condition != next_condition and (current_condition, next_condition) not in \
                {('No ice', 'High probability of Cloud'), ("High probability of Cloud", "No ice")}:

            # Interpolate the date between the current interval_to and the next interval_from
            current_to = pd.to_datetime(data.iloc[i]['interval_to'])
            next_from = pd.to_datetime(data.iloc[i + 1]['interval_from'])
            midpoint_date = current_to + (next_from - current_to) / 2
            ice_run_dates.append(midpoint_date.strftime('%Y-%m-%d'))

    return ice_run_dates


if __name__ == "__main__":

    # updates all the .csv files including information about images taken from sentinel hub
    update_all_polynomials()

    # location for an given box and returns the closest box cordinates and the id of the box
    lon, lat = 10.66, 60.95     # mjos
    closest_box, box_id = box_funcitons.get_closest_bbox_and_id(lon, lat)

    
    data        = read_from_csv(f"bbox_sat_data/lake_{box_id}")
    icerundates = get_ice_run_dates(data)

    print("33")
    pass
