import unittest
import pandas as pd
import datetime as dt

from server.Sentinelhub import getAreaInfo


class MyTestCase(unittest.TestCase):
    def test_stats_to_df(self):
        stats_data = {
            "data": [
                {
                    "interval": {"from": "2023-01-01T00:00:00Z", "to": "2023-01-02T00:00:00Z"},
                    "outputs": {
                        "default": {
                            "bands": {
                                "B0": {
                                    "stats": {
                                        "sampleCount": 100,
                                        "noDataCount": 0,
                                        "min": 0.0,
                                        "max": 1.0,
                                        "mean": 0.5,
                                        "stDev": 0.1,
                                        "percentiles": {"10.0": 0.1, "90.0": 0.9}
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        }

        expected_df = pd.DataFrame({
            "interval_from": [dt.date(2023, 1, 1)],
            "interval_to": [dt.date(2023, 1, 2)],
            "default_B0_sampleCount": [100],
            "default_B0_noDataCount": [0],
            "default_B0_min": [0.0],
            "default_B0_max": [1.0],
            "default_B0_mean": [0.5],
            "default_B0_stDev": [0.1],
            "default_B0_percentiles_10.0": [0.1],
            "default_B0_percentiles_90.0": [0.9]
        })



        result_df = getAreaInfo.stats_to_df(stats_data)
        pd.testing.assert_frame_equal(result_df, expected_df)

    def test_classify_ice(self):
        data = pd.DataFrame({
            'default_B0_stDev': [36, 10, 5, 8],
            'default_B0_noDataCount': [500, 500, 500, 500]
        })
        expected_conditions = ['High probability of Cloud', 'No ice', 'Ice', 'No ice']
        result = getAreaInfo.classify_ice(data)
        self.assertEqual(result['ice_condition'].tolist(), expected_conditions)

    def test_get_last_date(self):
        data = pd.DataFrame({
            'interval_to': ['2023-01-01', '2023-02-01', '2023-03-01']
        })
        expected_last_date = '2023-03-01'
        self.assertEqual(getAreaInfo.get_last_date(data), expected_last_date)

        expected_last_date_none = (
        dt.datetime.now().year - 1 if dt.datetime.now().month < 8 else dt.datetime.now().year, 8, 1)
        self.assertEqual(getAreaInfo.get_last_date(None), dt.datetime(*expected_last_date_none).strftime("%Y-%m-%d"))

    def test_get_ice_run_dates(self):
        data = pd.DataFrame({
            'interval_to': ['2023-01-01', '2023-02-01', '2023-03-01', '2023-04-01'],
            'interval_from': ['2023-01-02', '2023-02-02', '2023-03-02', '2023-04-02'],
            'ice_condition': ['Ice', 'No ice', 'Ice', 'High probability of Cloud']
        })
        expected_dates = ['2023-01-17', '2023-02-15', '2023-03-17']
        self.assertEqual(getAreaInfo.get_ice_run_dates(data), expected_dates)

if __name__ == '__main__':
    unittest.main()
