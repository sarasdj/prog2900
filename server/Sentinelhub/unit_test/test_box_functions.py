import unittest
from unittest.mock import patch, mock_open
import json
from sentinelhub import BBox, CRS

from server.Sentinelhub import box_funcitons

# Sample data for mocking the box.json content
mock_box_json = {
    "type": "MultiPolygon",
    "coordinates": [
        [
            [
                [10.0, 60.0],
                [10.0, 61.0],
                [11.0, 61.0],
                [11.0, 60.0]
            ]
        ],
        [
            [
                [12.0, 62.0],
                [12.0, 63.0],
                [13.0, 63.0],
                [13.0, 62.0]
            ]
        ]
    ]
}

class TestBoxFunctions(unittest.TestCase):

    @patch('server.Sentinelhub.box_funcitons.open', new_callable=mock_open, read_data=json.dumps(mock_box_json))
    @patch('server.Sentinelhub.box_funcitons.os.path.realpath', return_value='/path/to/fake/dir')
    def test_get_all_box(self, mock_realpath, mock_open):
        boxes = box_funcitons.get_all_box()
        expected_boxes = [
            [[10.0, 60.0], [10.0, 61.0], [11.0, 61.0], [11.0, 60.0]],
            [[12.0, 62.0], [12.0, 63.0], [13.0, 63.0], [13.0, 62.0]]
        ]
        self.assertEqual(boxes, expected_boxes)

    def test_create_BBox(self):
        box = [[10.0, 60.0], [10.0, 61.0], [11.0, 61.0], [11.0, 60.0]]
        bbox = box_funcitons.create_BBox(box)
        expected_bbox = BBox((10.0, 60.0, 11.0, 61.0), CRS.WGS84)
        self.assertEqual(bbox, expected_bbox)

    @patch('server.Sentinelhub.box_funcitons.get_all_box')
    def test_get_all_bbox(self, mock_get_all_box):
        mock_get_all_box.return_value = [
            [[10.0, 60.0], [10.0, 61.0], [11.0, 61.0], [11.0, 60.0]],
            [[12.0, 62.0], [12.0, 63.0], [13.0, 63.0], [13.0, 62.0]]
        ]
        bboxes = box_funcitons.get_all_bbox()
        expected_bboxes = [
            BBox((10.0, 60.0, 11.0, 61.0), CRS.WGS84),
            BBox((12.0, 62.0, 13.0, 63.0), CRS.WGS84)
        ]
        self.assertEqual(bboxes, expected_bboxes)

    def test_get_distance(self):
        box = [[10.0, 60.0], [10.0, 61.0], [11.0, 61.0], [11.0, 60.0]]
        point = (10.5, 60.5)
        distance = box_funcitons.get_distance(box, point)
        self.assertEqual(distance, 0.0)

        point_outside = (15.0, 65.0)
        distance_outside = box_funcitons.get_distance(box, point_outside)
        self.assertAlmostEqual(distance_outside, 5.656, places=2)

    @patch('server.Sentinelhub.box_funcitons.get_all_box')
    def test_get_closest_bbox_and_id(self, mock_get_all_box):
        mock_get_all_box.return_value = [
            [[10.0, 60.0], [10.0, 61.0], [11.0, 61.0], [11.0, 60.0]],
            [[12.0, 62.0], [12.0, 63.0], [13.0, 63.0], [13.0, 62.0]]
        ]
        bbox, idx = box_funcitons.get_closest_bbox_and_id(10.5, 60.5)
        expected_bbox = BBox((10.0, 60.0, 11.0, 61.0), CRS.WGS84)
        self.assertEqual(bbox, expected_bbox)
        self.assertEqual(idx, 0)

        bbox, idx = box_funcitons.get_closest_bbox_and_id(12.5, 62.5)
        expected_bbox = BBox((12.0, 62.0, 13.0, 63.0), CRS.WGS84)
        self.assertEqual(bbox, expected_bbox)
        self.assertEqual(idx, 1)

if __name__ == '__main__':
    unittest.main()