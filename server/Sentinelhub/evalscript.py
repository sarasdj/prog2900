
ndmi = """
    
    //VERSION=3
    
    function setup() {
      return {
        input: ["B08", "B11", "dataMask"], 
        output: [{
          id: "default",
          bands: 1, 
          sampleType: 'UINT8'
        }, {
          id: "dataMask",
          bands: 1,
          sampleType: 'UINT8'
        }]
      };
    }
    
    function evaluatePixel(samples) {
      let ndmi = calculateIndex(samples.B08, samples.B11); // (NIR - SWIR1) / (NIR + SWIR1)
      ndmi = normalizeIndex(ndmi); // Normalize to 0-1 scale
    
      return {
        default: [colorScale(ndmi), colorScale(ndmi), colorScale(ndmi), 255], // RGB representation using NDMI
        dataMask: [samples.dataMask]
      };
    }
    
    function calculateIndex(band1, band2) {
      return (band1 - band2) / (band1 + band2);
    }
    
    function normalizeIndex(value) {
      return (value + 1) / 2; // Transform from -1 to 1 range to 0 to 1
    }
    
    function colorScale(value) {
      return Math.round(value * 255); // Scale index values to 0-255 for RGB visualization
    }
    """

true_color = """
        //VERSION=3

        function setup() {
            return {
                input: [{
                    bands: ["B02", "B03", "B04"]
                }],
                output: {
                    bands: 3
                }
            };
        }

        function evaluatePixel(sample) {
            return [sample.B04, sample.B03, sample.B02];
        }
    """

error = """
    SHOULD GIVE ERROR
"""

lake_ice_index = """
//VERSION=3

function setup() {
  return {
    input: ["B04", "B08", "B11", "B12", "dataMask"], 
    output: [{
      id: "default",
      bands: 1,
      sampleType: 'UINT8'
    }, {
      id: "dataMask",
      bands: 1,
      sampleType: 'UINT8'
    }]
  };
}

function evaluatePixel(sample) {
  var Red = sample.B04;  
  var SWIR2 = sample.B12; 
  var NIR = sample.B08;  
  var SWIR1 = sample.B11;

  var contrast = 120
  var result = ((Red + SWIR2) / (NIR + SWIR1)) * contrast; 

  if (sample.dataMask === 1) {
    return {
      default: [result], 
      dataMask: [1]      
    };
  } else {
    return {
      default: [0],
      dataMask: [0] // Output 0 to indicate invalid data
    };
  }
}
"""
