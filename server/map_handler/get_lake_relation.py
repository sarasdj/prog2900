from server.consts import LAKE_RELATIONS_PATH


def get_map_data_handler(self, file_name: str, measurement: bool):
    status_code, map_data = fetch_data(file_name, measurement)

    # Set HTTP headers
    self.send_response(status_code)
    self.send_header("Content-type", "application/json")
    self.end_headers()

    # Write contents of the JSON file to response
    self.wfile.write(map_data.encode('utf-8'))


def fetch_data(file_name: str, measurement: bool) -> (int, str):
    """
        Returns the contents of the requested map file.

            Parameters:
                file_name (str): The name of the requested file/lake
                measurement (bool): Whether the file is of type _measurements.json or _div.json
            Returns:
                (int, str): A HTTP status code and the requested data
    """
    try:
        if measurement:
            file_type = "_measurements.json"
        else:
            file_type = "_div.json"

        # Extract contents from JSON file
        with open(LAKE_RELATIONS_PATH + file_name + file_type, "r") as file:
            data = file.read()

        return 200, data

    except FileNotFoundError:
        return 404, []
    except Exception:
        return 500, []

