from server.map_handler.update_measurements import update_measurements

def test_update_measurements_invalid_lake() -> None:
    test_lake_name = "test_lake"

    status_code, _ = update_measurements(test_lake_name)

    assert status_code == 404

def test_update_measurements_valid_lake() -> None:
    test_lake_name = "Skumsjøen"

    status_code, _ = update_measurements(test_lake_name)

    assert status_code == 200
