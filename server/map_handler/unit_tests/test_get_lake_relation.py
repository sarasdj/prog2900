from server.map_handler.get_lake_relation import fetch_data


def test_fetch_data_true() -> None:
    test_lake_name = "Mjøsa"

    status_code, _ = fetch_data(test_lake_name, True)

    assert status_code == 200


def test_fetch_data_false() -> None:
    test_lake_name = "Mjøsa"

    status_code, _ = fetch_data(test_lake_name, False)

    assert status_code == 200


def test_fetch_data_no_lake() -> None:
    test_lake_name = "non-existent-lake"

    status_code, _ = fetch_data(test_lake_name, False)

    assert status_code == 404
