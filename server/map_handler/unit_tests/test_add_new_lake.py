import os
import json
from shapely.geometry import Polygon, LineString

from server.consts import LAKE_RELATIONS_PATH
from server.map_handler.add_new_lake import create_grid, save_all_data, cut_map


def test_cut_map() -> None:
    test_cursor = None
    test_lake_name = "test_lake"

    status_code, _ = cut_map(test_cursor, test_lake_name, 1)

    assert status_code == 500


def test_create_grid_default() -> None:
    # Create a simple square polygon
    test_poly = Polygon([(0, 0), (1, 0), (1, 1), (0, 1)])

    # Define the expected grid output
    coordinates = [[(0, 0), (1, 0)], [(0, 0.4), (1, 0.4)], [(0, 0.8), (1, 0.8)],
                   [(0, 0), (0, 1)], [(0.5, 0), (0.5, 1)], [(1, 0), (1, 1)]]

    expected = [LineString(coords) for coords in coordinates]

    grid = create_grid(test_poly, 0.5, 0.4)
    assert grid == expected


def test_write_json_to_file() -> None:
    # Define some test data
    test_lake_name = "test_lake"
    test_data = {
        'test-field1': 'test-value1',
        'test-field2': [
            "test-value2",
            "test-value3"
        ],
    }
    test_centers = [
        (0, (59.24678, 67.2478)),
        (1, (78.7897, 62.43578)),
    ]
    test_path = LAKE_RELATIONS_PATH + '/' + test_lake_name

    # Call the function that is being tested
    save_all_data(test_lake_name, test_data, test_centers)

    # Try to read the data from the newly created file
    with open(test_path + '_div.json', 'r') as f:
        result = json.load(f)

    assert result == test_data

    # Remove the files created during the tests
    os.remove(test_path + '_div.json')
    os.remove(test_path + '_centers.txt')