
from icemodellingscripts import plotlakes as pl
import server.ModelFromNVE.setenvironment as se  # <- alle instannser er se.plotfolder
import csv
import json
import utm
from datetime import datetime

def get_ice_thickness_given_date(date, modeldata8, modeldata9):
    """
    This function finds the ice thickness for a given date by matching the date in modeldata8
    and returning the corresponding value in modeldata9.

    Parameters:
        date: The specific date for which the ice thickness is needed.
        modeldata8: A list of dates.
        modeldata9: A list of ice thickness values corresponding to the dates in modeldata8.

    Returns:
        :Float      - The ice thickness for the given date, or None if the date is not found.
    """
    try:
        # Find the index of the given date in modeldata8
        num = modeldata8.index(date)
        # Return the corresponding ice thickness from modeldata9
        return modeldata9[num]
    except ValueError:
        # If the date is not found in modeldata8, return None
        return None

# no need fro this one
def get_icethickness_date_from_csv(date, location_name, plot_folder = se.plot_folder):
    """
       Reads ice thickness data from a CSV file for a given date and location, then returns the ice thickness for that date.

       Parameters:
           date: The specific date for which the ice thickness is needed, as a datetime object.
           location_name: The name of the location, used to identify the correct CSV file.
           plot_folder: The folder where the CSV files are stored. Defaults to the plot_folder defined in the 'se' module.

       Returns:
        :Float      - The ice thickness for the given date as a float, or None if the date is not found in the CSV.
       """
    # Format the date as DD.MM.YY for matching with the dates in the CSV
    date_str = date.strftime("%d.%m.%y")
    # Construct the path to the CSV file for the given location
    csv_path = f"{plot_folder}{location_name}_is_fast.csv"

    try:
        with open(csv_path, mode='r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                if row[0] == date_str:
                    return float(row[1].replace(',', '.'))
    except FileNotFoundError:
        print(f"No data file found for {location_name} inside: {plot_folder}.")
    except Exception as e:
        print(f"An error occurred: {e}")

    # Return None if the date is not found or in case of an error
    return None

# no need fro this one
def plot_lakes_simplified(x, y, altitude, location_name, year, plot_folder = se.plot_folder, icerun_dates = []):
    """
    Parameters:
        x: UTM-east zone 33
        y: UTM-north zone 33
        altitude: altitude in m
        location_name: just name
        year: 2023 = end og 2023 + start of 2024
        param icerun_dates: dates when an ice run has cleaned the river for ice.   - Ice reset to ice free


    Returns:
        :modeldata              - List of data for:
          :modeldata[0]        - date for thickest single layer
          :modeldata[1]        - thickest single layer
          :modeldata[2]        - slush_ice thickness in thickest single layer
          :modeldata[3]        - black_ice thickness in thickest single layer
          :modeldata[4]        - date for biggest sum of solid ice layers
          :modeldata[5]        - biggest sum of solid ice layers
          :modeldata[6]        - date for biggest sum of all ice layers (ice thickness)
          :modeldata[7]        - biggest sum of all ice layers (ice thickness)
          :modeldata[8]        - list of dates with ice calculations
          :modeldata[9]        - list of sum of solid ice at each ice calculation
          :modeldata[10]       - list of dates with input air temperatures
          :modeldata[11]       - list of input air temperatures
    """
    ####################################################################################################################
    # Her er antatte vanntemperaturer ved gitte lufttemperaturer
    # En array for hver måned med kopier i endene.
    # Dataene er derfor desember, januar, februar, .... , desember, januar. 14 elementer
    # Merk at mai-november er tulledata. Dette er utenfor isleggingen i Numedalslågen
    awt = [[[-20, 0], [-15, 0], [-10, 0], [-5, 0.5], [0, 1.0], [5, 1.5], [10, 2.0], [15, 2.2], [20, 2.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.1], [5, 0.2], [10, 0.2], [15, 0.3], [20, 0.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.2], [5, 0.3], [10, 0.3], [15, 0.4], [20, 0.5]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.3], [0, 0.8], [5, 1.6], [10, 2.6], [15, 2.8], [20, 3.0]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.4], [0, 1.9], [5, 3.3], [10, 4.5], [15, 5.0], [20, 5.5]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.7], [0, 1.9], [5, 3.3], [10, 4.5], [15, 5.0], [20, 5.5]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.6], [0, 0.8], [5, 1.6], [10, 2.6], [15, 2.8], [20, 3.0]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.5], [0, 1.0], [5, 1.5], [10, 2.0], [15, 2.2], [20, 2.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.1], [5, 0.2], [10, 0.2], [15, 0.3], [20, 0.4]]]

    mf = 0.04
    """
    awt = []
    mf = []
    """
    ####################################################################################################################

    fdate = str(year) + '-10-01'
    ldate = str(year + 1) + '-05-31'

    return pl.run_newsite(fdate, ldate, 10, 1, forcing='grid', location_name=location_name,
                             plot_folder=plot_folder, x=x, y=y, altitude=altitude, icerun_dates=icerun_dates,
                             melt_factor=mf, air_water_temp=awt)

# no need fro this one
def update_csv(location_name, dates_ice, sum_ice, dates_air, air_temp, folder = se.plot_folder):
    # Create csv-file to store solid ice data
    outf4 = open('{0}{1}_is_fast.csv'.format(folder, location_name), 'w')
    # Create csv-file to store air temperature data
    outf5 = open('{0}{1}_lufttemp.csv'.format(folder, location_name), 'w')

    # Save solid ice data
    for k in range(len(dates_ice)):
        outf4.write(dates_ice[k].strftime("%d.%m.%y") + ';' + f'{sum_ice[k]:6.2f}'.replace('.', ',') + '\n')

    # Save air temperature data
    for k in range(len(dates_air)):
        outf5.write(dates_air[k].strftime("%d.%m.%y") + ';' + f'{air_temp[k]:6.1f}'.replace('.', ',') + '\n')

    outf4.close()
    outf5.close()



# no need fro this one
def update_json(location_name, dates_ice, sum_ice, dates_air, air_temp, folder=se.plot_folder):
    # Prepare data for solid ice in JSON format
    ice_data = [{"date": date.strftime("%d.%m.%y"), "sum_ice": f'{value:6.2f}'.replace('.', ',')}
                for date, value in
                zip(dates_ice, sum_ice)]
    # Prepare data for air temperature in JSON format
    air_data = [{"date": date.strftime("%d.%m.%y"), "air_temp": f'{value:6.1f}'.replace('.', ',')}
                for date, value in
                zip(dates_air, air_temp)]

    # Save solid ice data as JSON
    with open(f'{folder}{location_name}_is_fast.json', 'w') as outf4:
        json.dump(ice_data, outf4, indent=4)

    # Save air temperature data as JSON
    with open(f'{folder}{location_name}_lufttemp.json', 'w') as outf5:
        json.dump(air_data, outf5, indent=4)

def update_json(location_name, data, folder = se.plot_folder):
    #temp = [{"id": data[0], "x":data[1], "y":data[2], "date": data[3].strftime("%d.%m.%y"), "total_ice":data[4]}
    temp = [{"sub_div_id": str(i), "x": f"{x:.4f}", "y": f"{y:.4f}", "date": str(datetime.strptime(date, '%Y-%m-%d')), "total_ice": total_ice}
            for i, x, y, date, total_ice in data]

    with open(f'{folder}{location_name}_is_test.json', 'w') as outf4:
        json.dump(temp, outf4, indent=4)

def write_ice_info_for_points_based_on_date(cordinates_and_ids, date, altitude, location_name,
                                            plot_folder = se.plot_folder, icerun_dates = []):
    """

    Args:
        cordinates_and_ids: [(id, [x,y]), (id, [x,y])]
        date:

    Returns:
        Nothing, wirtes a json with that is structured like this:
        {
            sub_div_id : ...
            x : ...
            y : ...
            date : ...
            total_ice : ...
        }
        ...
        {
    """

    data = []

    year = int(date[0:4])    # get from date
    fdate = str(year) + '-10-01'
    ldate = str(year + 1) + '-05-31'

    ####################################################################################################################
    # Her er antatte vanntemperaturer ved gitte lufttemperaturer
    # En array for hver måned med kopier i endene.
    # Dataene er derfor desember, januar, februar, .... , desember, januar. 14 elementer
    # Merk at mai-november er tulledata. Dette er utenfor isleggingen i Numedalslågen
    awt = [[[-20, 0], [-15, 0], [-10, 0], [-5, 0.5], [0, 1.0], [5, 1.5], [10, 2.0], [15, 2.2], [20, 2.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.1], [5, 0.2], [10, 0.2], [15, 0.3], [20, 0.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.2], [5, 0.3], [10, 0.3], [15, 0.4], [20, 0.5]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.3], [0, 0.8], [5, 1.6], [10, 2.6], [15, 2.8], [20, 3.0]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.4], [0, 1.9], [5, 3.3], [10, 4.5], [15, 5.0], [20, 5.5]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.7], [0, 1.9], [5, 3.3], [10, 4.5], [15, 5.0], [20, 5.5]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.6], [0, 0.8], [5, 1.6], [10, 2.6], [15, 2.8], [20, 3.0]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.5], [0, 1.0], [5, 1.5], [10, 2.0], [15, 2.2], [20, 2.4]], \
           [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.1], [5, 0.2], [10, 0.2], [15, 0.3], [20, 0.4]]]

    mf = 0.04
    """
    awt = []
    mf = []
    """
    ####################################################################################################################

    # for every id/x/y do prognose on are x y
    for i in cordinates_and_ids:
        x = i[1][0]
        y = i[1][1]
        cords = utm.from_latlon(y, x, 33)

        temp = pl.run_newsite(fdate, ldate, 10, 1, forcing='grid', location_name=location_name + f"_sib_div_id_{i[0]}",
                             plot_folder=plot_folder, x=int(cords[0]), y=int(cords[1]), altitude=altitude,
                             icerun_dates=icerun_dates, make_plots=True, melt_factor=mf, air_water_temp=awt)
        ## handle plots ??? how iduno

        data.append([i[0], x, y, date, get_ice_thickness_given_date(date, temp[8], temp[9])])

    update_json(location_name, data, plot_folder)

    return

if __name__ == "__main__":
    location_name = 'Stjørdal'
    se.plot_folder += 'Exemple\\'

    # https://www.kartverket.no/til-lands/posisjon/transformere-koordinater-enkeltvis for å finne x og y
    data = plot_lakes_simplified(316405, 7042107, 33, location_name, 2023, plot_folder=se.plot_folder)
    #data = plot_lakes_simplified(266708, 6749365, 123, "Mjøsa", 2023, plot_folder=se.plot_folder)

    mjos = utm.to_latlon(266708, 6749365, 33, northern=True)

    print(f"Mjøsa kordinater : {mjos}" )
    print(f"Mjøsa kordinater : {utm.from_latlon(mjos[0], mjos[1], 33)}" )

    update_csv(location_name, data[8], data[9], data[10], data[11], se.plot_folder)
    #update_json(location_name, data[8], data[9], data[10], data[11], se.plot_folder)

    print(f"Total ice thiccness for date: {data[8][57]} is {get_icethickness_date_from_csv(data[8][57], location_name, se.plot_folder)} or",
          f"{get_ice_thickness_given_date(data[8][57], data[8], data[9])}")

    temp = [(1, [10.709985478463118, 60.810991171403316]), (2, [10.709985478463118, 60.850991171403316])]
    write_ice_info_for_points_based_on_date(temp, '2023-01-22', 123, 'Mjøsa', se.plot_folder)


    pass