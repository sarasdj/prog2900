import copy
import datetime as dt
import utm
import os
import json
import server.ModelFromNVE.setenvironment as se
from server.ModelFromNVE.icemodelling import parameterization as dp, icethickness as it, weatherelement as we, \
    ice as ice
from server.ModelFromNVE.utilities import makeplots as pts, getgts as gts, getwsklima as gws
from server.Sentinelhub import box_funcitons, getAreaInfo, sentinel_config


def ice_prognosis_raw_data(to_date=None, sub_div_id=0, x=10.70, y=60.81,
                           altitude=0, awt=[], mf=0, icerun_dates=[]):
    """

    Args:
        to_date:    the end date for prognosis season, defaults to seven days into the future
        sub_div_id:
        x:              lat
        y:              lon
        altitude:       in meters
        awt:            list of avrage water temperatures
        mf:             melt factor used by NVE, not much info on this variable
        icerun_dates:   date for ice of the lake

    Returns:
        a list of data at this format:
        [
            [
                "Date":             date.strftime("%Y-%m-%d"),
                "Slush ice (m)":    round(slush, 3),
                "Black ice (m)":    round(black, 3),
                "Total ice (m)":    round(total, 3),
                "Snow depth (m)":   round(snow2, 3),
                "Total snow (m)":   round(sno_tot2, 3),
                "Cloud cover":      round(cc2, 3),
                "Temperature (c)":  round(temp2, 3)
            ]
            ...
            [
                ...
            ]
        ]


    """
    current_date = dt.datetime.now()

    if current_date.month < 10:
        from_date = dt.datetime(current_date.year - 1, 10, 1)
    else:
        from_date = dt.datetime(current_date.year, 10, 1)

    if to_date is None or to_date > dt.datetime.now():
        to_date = dt.datetime.now() + dt.timedelta(days=7)

    tyear, tmonth, tday = from_date.year, from_date.month, from_date.day
    if tmonth > 7 and tday < 7:
        tyear += 1

    first_ice = ice.IceColumn(from_date, [])
    first_ice.add_metadata('LocationName', sub_div_id)  # Using sub_div_id as the location name for metadata
    observed_ice = [first_ice]

    # make the x and y into utm 33 from lon lat
    cords = utm.from_latlon(x, y, 33)
    x, y = int(cords[0]), int(cords[1])

    # check if utm is valid
    if validate_cords(x, y) is False:
        return None

    gridTemp = gts.getgts(x, y, 'tm', from_date, to_date)
    gridSno = gts.getgts(x, y, 'sdfsw', from_date, to_date)
    gridSnoTot = gts.getgts(x, y, 'sd', from_date, to_date)

    # In steep terrain may the grid altitude be different from the wanted height. Adjust for that.
    gridTempNewElevation = we.adjust_temperature_to_new_altitude(gridTemp, altitude)

    temp, date = we.strip_metadata(gridTempNewElevation, get_date_times=True)
    sno = we.strip_metadata(gridSno)
    sno_tot = we.strip_metadata(gridSnoTot)
    cc = dp.clouds_from_precipitation(sno)

    air_temp_date = []
    air_temp_value = []
    for i in range(len(gridTemp)):
        air_temp_date.append(gridTemp[i].Date)
        air_temp_value.append(gridTemp[i].Value)

    if len(awt) > 0:
        calculated_ice = None  # TODO
    else:
        calculated_ice = it.calculate_ice_cover_air_temp(copy.deepcopy(first_ice), date, temp, sno, cloud_cover=cc,
                                                         icerun_dates=icerun_dates)

    ###
    # cumulated ammount of each ice type at a given date
    slush_ice = []
    black_ice = []
    total = []
    total_ice = []
    dates = []

    for i in calculated_ice:
        ice_type = -10
        black_ice_daily = 0
        slush_ice_daily = 0
        all_layer_ice_daily = 0
        all_layer_daily = 0
        change = False
        date = i.date
        for j in i.column:
            ice_type = j.get_enum()
            ice_height = j.height
            if ice_type == 10:
                black_ice_daily += ice_height
            elif ice_type == 11:
                slush_ice_daily += ice_height
            all_layer_ice_daily += ice_height

        dates.append(date)
        slush_ice.append(slush_ice_daily)
        black_ice.append(black_ice_daily)
        total_ice.append(all_layer_ice_daily)

    data = []

    for date, slush, black, total, snow2, sno_tot2, cc2, temp2 in zip(dates, slush_ice, black_ice, total_ice, sno,
                                                                      sno_tot, cc, temp):
        daily_data = {
            "Date": date.strftime("%Y-%m-%d"),
            "Slush ice (m)": round(slush, 3),
            "Black ice (m)": round(black, 3),
            "Total ice (m)": round(total, 3),
            "Snow depth (m)": round(snow2, 3),
            "Total snow (m)": round(sno_tot2, 3),
            "Cloud cover": round(cc2, 3),
            "Temperature (c)": round(temp2, 3)
        }
        data.append(daily_data)

    return data
    # return [sub_div_id, x, y, data]


def validate_cords(easting, northing):
    """
    Args:
        easting:
        northing:

    Returns:
        if the easting and northing is not acceptable at utm 33 returns False else true

    """

    if not (100000 <= easting <= 900000) or not (0 <= northing <= 10000000):
        return False

    else:
        return True



def get_raw_dates(data, from_date=None, to_date=None):
    """
    Args:
        data:       data from ice_prognosis_raw_data
        from_date:  if not given default to 4 days into future
        to_date:    if not given default to 3 days into the past

    Returns:
        returns data for the specified time slot
    """
    if data is None:
        return []

    if from_date is None:
        from_date = (dt.datetime.now() - dt.timedelta(days=3)).strftime("%Y-%m-%d")
    if to_date is None:
        to_date = (dt.datetime.now() + dt.timedelta(days=4)).strftime("%Y-%m-%d")

    filtred_data = [entry for entry in data if from_date <= entry["Date"] <= to_date]
    return filtred_data


def format_dates(date_list):
    formatted_dates = []
    for date_string in date_list:
        try:
            before = dt.datetime.strptime(date_string, '%Y-%m-%d')
            after = before.strftime('%d.%m.%Y')
            formatted_dates.append(after)
        except ValueError as e:
            print(f"Error processing date {date_string}: {e}")
    return formatted_dates


# change to take a list of data with an sub div id first followed by data [sub_div_id, data]
def jsonify_data(data, name="temp", location=se.plot_folder):
    """

    Args:
        data:       the data to be put into file
        name:       name of file -> data_{name}.json
        location:   location, default to se.plot_folder

    Returns:
        creates a .json file

    """
    os.makedirs(location, exist_ok=True)
    file_path = os.path.join(location, f"data_{name}.json")

    try:
        with open(file_path, 'w', encoding='utf-8') as json_file:
            json.dump(data, json_file, indent=4)
        print(f"Data successfully saved to {file_path}")
    except Exception as e:
        print(f"Failed to save data to JSON file. Error: {e}")


def jsonify_data_sub_div_ids(lake_name, sub_div_and_data, location=se.plot_folder):
    """

    Args:
        lake_name:          put as file name {lake_name}_sub_div.json
        sub_div_and_data:   all data at this format [(id, data) ... (...)]
        location:           plot folder defaults to se.plot_folder

    Returns:
        creates a .json file

    """
    aggregated_data = {entry[0]: entry[1] for entry in sub_div_and_data}

    os.makedirs(location, exist_ok=True)

    filename = f"{lake_name}_sub_div.json"
    file_path = os.path.join(location, filename)

    try:
        with open(file_path, 'w', encoding='utf-8') as json_file:
            json.dump(aggregated_data, json_file, indent=4)
        print(f"All data successfully saved to {file_path}")
    except Exception as e:
        print(f"Failed to save data to JSON file. Error: {e}")


# def expose_data()
def get_subdiv_ids_n_cords(file_path):
    """

    Args:
        file_path: path of the file with ids lat lon values

    Returns:
        a list of ids lat lon

    """
    # reads file and gets all ids and cords at this format [(id, x, y), (id, x, y) ... ]
    id_list = []

    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            data = line.strip().split(',')
            if len(data) == 3:
                id_list.append((int(data[0]), float(data[1]), float(data[2])))

    return id_list


def update_data(from_date=None, to_date=None, lake_name="skumsjoen",
                     sub_divs_folder='../../map_handler/lake_relations/skumsjøen_centers.txt', update_all_bboxes=False):
        if update_all_bboxes:
            getAreaInfo.update_all_polynomials()

        sub_divs = get_subdiv_ids_n_cords(sub_divs_folder)
        filtered_data_for_dates = []

        # Iterate over each subdivision in the list
        for subdivision in sub_divs:
            sub_div_id = subdivision[0]
            x_coordinate = subdivision[1]
            y_coordinate = subdivision[2]

            closest_box, box_id = box_funcitons.get_closest_bbox_and_id(x_coordinate, y_coordinate)  # validate

            data = getAreaInfo.read_from_csv(f"bbox_sat_data/lake_index_{box_id}")
            icerundates = getAreaInfo.get_ice_run_dates(data)
            icerundates = format_dates(icerundates)

            # Fetch the raw data for the subdivision
            raw_data = ice_prognosis_raw_data(sub_div_id=sub_div_id, x=x_coordinate, y=y_coordinate,
                                              icerun_dates=icerundates)  # <- problem

            # Filter the raw data based on the specified date range
            filtered_data = get_raw_dates(raw_data, from_date, to_date)

            # Add the filtered data to the list
            filtered_data_for_dates.append((sub_div_id, filtered_data))

        filtered_data_for_dates.sort(key=lambda x: x[0])

        jsonify_data_sub_div_ids(lake_name, filtered_data_for_dates, location=se.plot_folder)
        return filtered_data_for_dates



if __name__ == "__main__":
    ''' 
    data = ice_prognosis_raw_data()

    from_date = "2024-01-10"
    to_date = "2024-01-20"
    filtered_dates = get_raw_dates(data, from_date, to_date)
    jsonify_data(filtered_dates)
    filtered_dates2 = get_raw_dates(data)
    
    all_will_be_one = [(1, filtered_dates2), [2, filtered_dates]]
    jsonify_data_sub_div_ids(all_will_be_one)
    '''



    from_date = "2024-01-10"
    to_date = "2024-01-20"

    update_data(from_date, to_date, lake_name="skumsjøen",
                sub_divs_folder='../../map_handler/lake_relations/skumsjøen_centers.txt', update_all_bboxes=True)

    # filtered_data_for_dates = [(i[0], get_raw_dates(ice_prognosis_raw_data(sub_div_id=i[0], x=i[1], y=i[2])), from_date, to_date) for i in sub_divs ]

    # without iceruns
    #filtered_data_for_dates1 = [(i[0], get_raw_dates(ice_prognosis_raw_data(sub_div_id=i[0], x=i[1], y=i[2]), from_date, to_date)) for i in sub_divs ]

    #getAreaInfo.update_all_polynomials()


    pass
