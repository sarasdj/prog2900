# -*- coding: utf-8 -*-
"""

"""
import copy
import datetime as dt
from server.ModelFromNVE.icemodelling import parameterization as dp, icethickness as it, weatherelement as we, \
    ice as ice
import server.ModelFromNVE.setenvironment as se         #   <- alle instannser er se.plotfolder
from server.ModelFromNVE.utilities import makeplots as pts, getgts as gts, getwsklima as gws

__author__ = 'ragnarekker'

##########################################################################################################



##########################################################################################################
'''
def run_otrovann_eb(startDate, endDate):

    location_name = 'Otrøvatnet v/Nystuen 971 moh'
    wsTemp = gws.getMetData(54710, 'TAM', startDate, endDate, 0, 'list')
    wsSno  = gws.getMetData(54710, 'SA',  startDate, endDate, 0, 'list')
    wsPrec = gws.getMetData(54710, 'RR',  startDate, endDate, 0, 'list')

    utm33_y = 6802070
    utm33_x = 130513

    temp, date = we.strip_metadata(wsTemp, get_date_times=True)
    sno_tot = we.strip_metadata(wsSno)
    prec_snow = dp.delta_snow_from_total_snow(sno_tot)
    prec = we.strip_metadata(wsPrec)
    cloud_cover = dp.clouds_from_precipitation(prec)
    wind = [const.avg_wind_const] * len(date)
    rel_hum = [const.rel_hum_air] * len(date)
    pressure_atm = [const.pressure_atm] * len(date)


    # available_elements = gws.getElementsFromTimeserieTypeStation(54710, 0, 'csv')
    observed_ice = gro.get_all_season_ice_on_location(location_name, startDate, endDate)

    ice_cover, energy_balance = it.calculate_ice_cover_eb(
        utm33_x, utm33_y, date, temp, prec, prec_snow, cloud_cover, wind, rel_hum=rel_hum, pressure_atm=pressure_atm,
        inn_column=copy.deepcopy(observed_ice[0]))

    # Need datetime objects from now on
    from_date = dt.datetime.strptime(startDate, "%Y-%m-%d")
    to_date = dt.datetime.strptime(endDate, "%Y-%m-%d")

    plot_filename = '{0}Ortovann MET EB {1}-{2}.png'.format(se.plot_folder, from_date.year, to_date.year)
    # pts.plot_ice_cover(ice_cover, observed_ice, date, temp, sno_tot, plot_filename)
    plot_filename = '{0}Ortovann MET with EB {1}-{2}.png'.format(se.plot_folder, from_date.year, to_date.year)
    pts.plot_ice_cover_eb(ice_cover, energy_balance, observed_ice, date, temp, sno_tot, plot_filename,
                       prec=prec, wind=wind, clouds=cloud_cover)


def run_semsvann_eb(startDate, endDate):
    # TODO: get coordinates from the ObsLocation in regObs
    location_name = 'Semsvannet v/Lo 145 moh'

    wsTemp = gws.getMetData(19710, 'TAM', startDate, endDate, 0, 'list')
    wsSno = gws.getMetData(19710, 'SA', startDate, endDate, 0, 'list')
    wsPrec = gws.getMetData(19710, 'RR', startDate, endDate, 0, 'list')
    wsWind = gws.getMetData(18700, 'FFM', startDate, endDate, 0, 'list')
    wsCC = gws.getMetData(18700, 'NNM', startDate, endDate, 0, 'list')

    utm33_y = 6644410
    utm33_x = 243940

    temp, date = we.strip_metadata(wsTemp, get_date_times=True)
    sno_tot = we.strip_metadata(wsSno)
    prec_snow = dp.delta_snow_from_total_snow(sno_tot)
    prec = we.strip_metadata(wsPrec)
    wind = we.strip_metadata(wsWind)
    cloud_cover = we.strip_metadata(wsCC)
    rel_hum = [const.rel_hum_air] * len(date)
    pressure_atm = [const.pressure_atm] * len(date)

    observed_ice = gro.get_all_season_ice_on_location(location_name, startDate, endDate)

    ice_cover, energy_balance = it.calculate_ice_cover_eb(
        utm33_x, utm33_y, date,
        temp, prec, prec_snow, cloud_cover=cloud_cover, wind=wind, rel_hum=rel_hum, pressure_atm=pressure_atm,
        inn_column=copy.deepcopy(observed_ice[0]))

    # Need datetime objects from now on
    from_date = dt.datetime.strptime(startDate, "%Y-%m-%d")
    to_date = dt.datetime.strptime(endDate, "%Y-%m-%d")

    plot_filename = '{0}Semsvann EB {1}-{2}.png'.format(se.plot_folder, from_date.year, to_date.year)
    # pts.plot_ice_cover(ice_cover, observed_ice, date, temp, sno_tot, plot_filename)
    plot_filename = '{0}Semsvann MET with EB {1}-{2}.png'.format(se.plot_folder, from_date.year, to_date.year)
    pts.plot_ice_cover_eb(ice_cover, energy_balance, observed_ice, date, temp, sno_tot, plot_filename, prec=prec, wind=wind, clouds=cloud_cover)
    #plot_filename = '{0}Semsvann MET with EB simple {1}-{2}.png'.format(plot_folder, from_date.year, to_date.year)
    #pts.plot_ice_cover_eb_simple(ice_cover, energy_balance, observed_ice, date, temp, sno_tot, plot_filename)


def run_semsvann(from_date, to_date, make_plots=True, plot_folder=se.plot_folder, forcing='grid'):
    """

    :param from_date:
    :param to_date:
    :param make_plots:
    :param plot_folder:
    :return:
    """

    location_name = 'Semsvann'
    regobs_location_id = 2227

    x = 243655
    y = 6644286
    altitude = 145

    met_stnr = 19710        # Asker (Sem)
    met_stnr_NNM = 18700    # Blindern

    year = '{0}-{1}'.format(from_date[0:4], to_date[2:4])

    observed_ice = gro.get_observations_on_location_id(regobs_location_id, year)
    first_ice = observed_ice[0]

    # Change dates to datetime. Some of the getdata modules require datetime
    from_date = dt.datetime.strptime(from_date, '%Y-%m-%d')
    to_date = dt.datetime.strptime(to_date, '%Y-%m-%d')

    # if to_date forward in time, make sure it doesnt go to far..
    if to_date > dt.datetime.now():
        to_date = dt.datetime.now() + dt.timedelta(days=7)

    if forcing == 'eKlima':

        wsTemp = gws.getMetData(met_stnr, 'TAM', from_date, to_date, 0, 'list')
        wsSnoTot = gws.getMetData(met_stnr, 'SA', from_date, to_date, 0, 'list')
        wsCC = gws.getMetData(met_stnr_NNM, 'NNM', from_date, to_date, 0, 'list')

        temp, date = we.strip_metadata(wsTemp, get_date_times=True)
        sno_tot = we.strip_metadata(wsSnoTot)
        sno = dp.delta_snow_from_total_snow(sno_tot)
        cc = we.strip_metadata(wsCC)

        plot_filename = '{0}_{1}_eklima.png'.format(location_name, year)

    elif forcing == 'grid':

        gridTemp = gts.getgts(x, y, 'tm', from_date, to_date)
        gridSno = gts.getgts(x, y, 'sdfsw', from_date, to_date)
        gridSnoTot = gts.getgts(x, y, 'sd', from_date, to_date)

        # Grid altitude and lake at same elevations.
        gridTempNewElevation = we.adjust_temperature_to_new_altitude(gridTemp, altitude)

        temp, date = we.strip_metadata(gridTempNewElevation, get_date_times=True)
        sno = we.strip_metadata(gridSno)
        sno_tot = we.strip_metadata(gridSnoTot)
        cc = dp.clouds_from_precipitation(sno)

        plot_filename = '{0}_{1}_grid.png'.format(location_name, year)

    else:
        temp, date = None, None
        sno = None
        sno_tot = None
        cc = None

        plot_filename = '{0}_{1}_no_forcing.png'.format(location_name, year)

    calculated_ice = it.calculate_ice_cover_air_temp(copy.deepcopy(first_ice), date, temp, sno, cloud_cover=cc)

    if make_plots:
        plot_path_and_filename = '{0}{1}'.format(plot_folder, plot_filename)
        pts.plot_ice_cover(calculated_ice, observed_ice, date, temp, sno, sno_tot, plot_path_and_filename)
'''

def run_newsite(from_date, to_date, startmonth, startday, make_plots=True, plot_folder=se.plot_folder, forcing='grid',
                location_name='Unknown', met_stnr=0, x=0, y=0, altitude=0, air_water_temp=[], melt_factor=0,
                icerun_dates=[]):
    """

    :param from_date:
    :param to_date:
    :param startmonth       - month to start ice building
    :param startday         - day to start ice building
    :param make_plots:      - make plots?
    :param plot_folder:     - Where to find folders
    :param forcing:         - eklima or grid. What type of input weather data
    :param location_name:   - Stedsnavn
    :param met_stnr:        - met.no station number                                  - used with eklima data
    :param x:               - UTM-east zone 33                                       - used with grid data
    :param y:               - UTM-north zone 33                                      - used with grid data
    :param altitude:        - altitude in m                                          - used to correct grid-data
    :param air_water_temp:  - array with air-water temp connection                   - To simulate melting from bottom
    :param melt_factor:     - water temp to melting in m/day                         - To simulate melting from bottom
    :param icerun_dates     - dates when an ice run has cleaned the river for ice.   - Ice reset to ice free

    :return:
    :modeldata              - List of data for:
      :modeldata[0]        - date for thickest single layer
      :modeldata[1]        - thickest single layer
      :modeldata[2]        - slush_ice thickness in thickest single layer
      :modeldata[3]        - black_ice thickness in thickest single layer
      :modeldata[4]        - date for biggest sum of solid ice layers
      :modeldata[5]        - biggest sum of solid ice layers
      :modeldata[6]        - date for biggest sum of all ice layers (ice thickness)
      :modeldata[7]        - biggest sum of all ice layers (ice thickness)
      :modeldata[8]        - list of dates with ice calculations
      :modeldata[9]        - list of sum of solid ice at each ice calculation
      :modeldata[10]       - list of dates with input air temperatures
      :modeldata[11]       - list of input air temperatures
    """

    tyear = int(from_date[0:4])
    tmonth = int(from_date[5:7])
    if tmonth > 7 and startmonth < 7:
        # We want to start ice onset after newyear, but the calculations starts before newyear
        tyear += 1
    first_ice = ice.IceColumn(dt.datetime(tyear, startmonth, startday), [])
    first_ice.add_metadata('LocationName', location_name)       # used when plotting
    observed_ice = [first_ice]

    year = '{0}-{1}'.format(from_date[0:4], to_date[2:4])

    # Change dates to datetime. Some of the getdata modules require datetime
    from_date = dt.datetime.strptime(from_date, '%Y-%m-%d')
    to_date = dt.datetime.strptime(to_date, '%Y-%m-%d')

    # if to_date forward in time, make sure it doesnt go to far..
    if to_date > dt.datetime.now():
        to_date = dt.datetime.now() + dt.timedelta(days=7)

    if forcing == 'eKlima':

        wsTemp = gws.getMetData(met_stnr, 'TAM', from_date, to_date, 0, 'list')
        gridSno = gts.getgts(x, y, 'sdfsw', from_date, to_date)
        gridSnoTot = gts.getgts(x, y, 'sd', from_date, to_date)

        temp, date = we.strip_metadata(wsTemp, get_date_times=True)
        sno = we.strip_metadata(gridSno)
        sno_tot = we.strip_metadata(gridSnoTot)
        cc = dp.clouds_from_precipitation(sno)

        plot_filename = '{0}_{1}_eklima.png'.format(location_name, year)

    elif forcing == 'grid':

        gridTemp = gts.getgts(x, y, 'tm', from_date, to_date)
        gridSno = gts.getgts(x, y, 'sdfsw', from_date, to_date)
        gridSnoTot = gts.getgts(x, y, 'sd', from_date, to_date)

        # In steep terrain may the grid altitude be different from the wanted height. Adjust for that.
        gridTempNewElevation = we.adjust_temperature_to_new_altitude(gridTemp, altitude)

        temp, date = we.strip_metadata(gridTempNewElevation, get_date_times=True)
        sno = we.strip_metadata(gridSno)
        sno_tot = we.strip_metadata(gridSnoTot)
        cc = dp.clouds_from_precipitation(sno)

        plot_filename = '{0}_{1}_grid.png'.format(location_name, year)

        # Put air temperature data in a list
        air_temp_date = []
        air_temp_value = []
        for i in range(len(gridTemp)):
            air_temp_date.append(gridTemp[i].Date)
            air_temp_value.append(gridTemp[i].Value)

    else:
        temp, date = None, None
        sno = None
        sno_tot = None
        cc = None

        plot_filename = '{0}_{1}_no_forcing.png'.format(location_name, year)

    # Calculate melting
    if len(air_water_temp) > 0:
        # Add melting from the bottom based on air temperatures
        # Dataene i air_water_temp er desember, januar, februar, .... , desember, januar. 14 elementer
        melt = []
        twt = 0
        n_awt = len(air_water_temp[0])
        for i in range(len(temp)):
            # Find month index
            mindex = date[i].month
            tday = date[i].day
            if tday > 30:
                # The most correct would be to use the correct daynumber in each month,
                # but this simplifies to almost correct value
                tday = 30
            # Find water temp from air temp
            if temp[i] <= air_water_temp[mindex][0][0]:
                # Mindre enn øvre lufttemperaturgrense
                # Ingen interpolering på lufttemperatur
                if tday <= 15:
                    utwt = air_water_temp[mindex][0][1]
                    ltwt = air_water_temp[mindex - 1][0][1]
                    # Interpoler på dato
                    twt = ltwt + (utwt - ltwt) * (15 + tday) / 30
                else:
                    utwt = air_water_temp[mindex + 1][0][1]
                    ltwt = air_water_temp[mindex][0][1]
                    # Interpoler på dato
                    twt = ltwt + (utwt - ltwt) * (tday - 15) / 30
            elif temp[i] >= air_water_temp[mindex][n_awt - 1][0]:
                # Større enn øvre lufttemperaturgrense
                # Ingen interpolering på lufttemperatur
                if tday <= 15:
                    utwt = air_water_temp[mindex][n_awt - 1][1]
                    ltwt = air_water_temp[mindex - 1][n_awt - 1][1]
                    # Interpoler på dato
                    twt = ltwt + (utwt - ltwt) * (15 + tday) / 30
                else:
                    utwt = air_water_temp[mindex + 1][n_awt - 1][1]
                    ltwt = air_water_temp[mindex][n_awt - 1][1]
                    # Interpoler på dato
                    twt = ltwt + (utwt - ltwt) * (tday - 15) / 30
            else:
                # Interpoler innenfor temperaturgrensene
                for j in range(n_awt):
                    if (temp[i] <= air_water_temp[mindex][j+1][0]):
                        if tday <= 15:
                            # Interpoler
                            utwt = air_water_temp[mindex][j][1] + \
                                (air_water_temp[mindex][j+1][1] - air_water_temp[mindex][j][1]) \
                                * ((temp[i] - air_water_temp[mindex][j][0]) \
                                / (air_water_temp[mindex][j+1][0] - air_water_temp[mindex][j][0]))
                            ltwt = air_water_temp[mindex - 1][j][1] + \
                                (air_water_temp[mindex - 1][j+1][1] - air_water_temp[mindex - 1][j][1]) \
                                * ((temp[i] - air_water_temp[mindex - 1][j][0]) \
                                / (air_water_temp[mindex - 1][j+1][0] - air_water_temp[mindex - 1][j][0]))
                            # Interpoler på dato
                            twt = ltwt + (utwt - ltwt) * (15 + tday) / 30
                        else:
                            # Interpoler
                            utwt = air_water_temp[mindex + 1][j][1] + \
                                (air_water_temp[mindex + 1][j+1][1] - air_water_temp[mindex + 1][j][1]) \
                                * ((temp[i] - air_water_temp[mindex + 1][j][0]) \
                                / (air_water_temp[mindex + 1][j+1][0] - air_water_temp[mindex + 1][j][0]))
                            ltwt = air_water_temp[mindex][j][1] + \
                                (air_water_temp[mindex][j+1][1] - air_water_temp[mindex][j][1]) \
                                * ((temp[i] - air_water_temp[mindex][j][0]) \
                                / (air_water_temp[mindex][j+1][0] - air_water_temp[mindex][j][0]))
                            # Interpoler på dato
                            twt = ltwt + (utwt - ltwt) * (tday - 15) / 30
                        break
            # Add to melt array
            melt.append(twt * melt_factor)

        # Save to array for check
        # Open an output file
        outf3 = open('{0}{1}_smelting.txt'.format(plot_folder, location_name), 'w', encoding='utf-8')
        outf3.write('maksdato   ')
        for i in range(len(temp)):
            outf3.write(str(date[i]) + f'{temp[i]:6.2f}' + f'{(melt[i]/melt_factor):6.2f}' + '\n')
        outf3.close()

        calculated_ice = it.calculate_ice_cover_air_temp_with_submelt(copy.deepcopy(first_ice), date, temp, melt, sno, cloud_cover=cc)
    else:
        # Ignore melting from the bottom
        calculated_ice = it.calculate_ice_cover_air_temp(copy.deepcopy(first_ice), date, temp, sno, cloud_cover=cc,
                                                         icerun_dates=icerun_dates)


    # Run through data and save largest total ice thickness
    maxonelayerthickness = 0
    slush_ice = 0
    black_ice = 0
    total_ice = 0
    total_solidice = 0
    maxdate = None
    solid_ice_date_list = []
    solid_ice_sum_list = []

    for i in range(len(calculated_ice)):
        picetype = -10   # Previous ice type. Use one not used
        talllayersum = 0
        talllayersolidsum = 0
        tlayersum = 0
        slush_ice_sum = 0
        black_ice_sum = 0
        maxchanged = False
        for j in range(len(calculated_ice[i].column)):
            ticetype = calculated_ice[i].column[j].get_enum()
            theight = calculated_ice[i].column[j].height
            talllayersum += theight
            if ticetype == 10 or ticetype == 11:
                if ticetype == 10:
                    black_ice_sum += theight
                elif ticetype == 11:
                    slush_ice_sum += theight
                talllayersolidsum += theight
                tlayersum += theight
                if tlayersum > maxonelayerthickness:
                    maxonelayerthickness = tlayersum
                    slush_ice = slush_ice_sum
                    black_ice = black_ice_sum
                    maxchanged = True
            else:
                tlayersum = 0
                slush_ice_sum = 0
                black_ice_sum = 0
        if maxchanged:
            # We have a new record
            maxdate = calculated_ice[i].date
        if talllayersum > total_ice:
            total_ice = talllayersum
            total_ice_date = calculated_ice[i].date
        if talllayersolidsum > total_solidice:
            total_solidice = talllayersolidsum
            total_solidice_date = calculated_ice[i].date

        # Save solid ice data to list
        solid_ice_date_list.append(calculated_ice[i].date)
        solid_ice_sum_list.append(talllayersolidsum)
    if make_plots:
        plot_path_and_filename = '{0}{1}'.format(plot_folder, plot_filename)
        pts.plot_ice_cover(calculated_ice, observed_ice, date, temp, sno, sno_tot, plot_path_and_filename)

    return (maxdate, maxonelayerthickness, slush_ice, black_ice, total_solidice_date,
            total_solidice, total_ice_date, total_ice, solid_ice_date_list, solid_ice_sum_list,
            air_temp_date, air_temp_value)


if __name__ == "__main__":

    #run_semsvann('2017-11-01', '2018-06-01')
    #run_semsvann('2016-11-01', '2017-06-01')
    #run_semsvann('2015-11-01', '2016-06-01')
    #run_semsvann('2014-11-01', '2015-06-01')

    #run_semsvann('2017-11-01', '2018-06-01', forcing='eKlima')
    #run_semsvann('2016-11-01', '2017-06-01', forcing='eKlima')
    #run_semsvann('2015-11-01', '2016-06-01', forcing='eKlima')

    #run_mosselva('2017-11-01', '2018-06-01')
    #run_mosselva('2016-11-01', '2017-06-01')
    #run_mosselva('2015-11-01', '2016-06-01')

    #run_newsite('2017-11-01', '2018-06-01', forcing='eKlima', location_name='Svene', met_stnr=28380)
    #run_newsite('2016-11-01', '2017-06-01', forcing='grid', location_name='Svene', x=195798 , y=6640743, altitude=175)
    #run_mosselva('2015-11-01', '2016-06-01', forcing='eKlima')

    # cap.calculate_and_plot_location('Semsvannet v/Lo 145 moh', '2016-10-01', '2017-07-01')
    # location_name = 'Beiarn'
    # se.plot_folder += 'Beiarn\\'
    location_name = 'Stjørdal'
    se.plot_folder += 'Stjørdal-2\\'

    # Open an output file for extremes
    # outf = open('{0}{1}_is_ekstremer.txt'.format(se.plot_folder, location_name), 'w')

    # Create csv-file to store solid ice data
    outf4 = open('{0}{1}_is_fast.csv'.format(se.plot_folder, location_name), 'w', encoding='utf-8')
    # Create csv-file to store air temperature data
    outf5 = open('{0}{1}_lufttemp.csv'.format(se.plot_folder, location_name), 'w', encoding='utf-8')

    # Overskrift
    outf.write('Maksdato - Dato for største tykkelse av ett lag solid is\n')
    outf.write('MEL      - Største tykkelse av ett lag solid is\n')
    outf.write('SØI      - Søpeistykkelsen i det tykkeste enkeltlaget\n')
    outf.write('STI      - Stålistykkelsen i det tykkeste enkeltlaget\n')
    outf.write('TSID     - Dato for største tykkelse av solid is summert\n')
    outf.write('TSI      - Største tykkelse av solid is summert\n')
    outf.write('TID      - Dato for største tykkelse av alle lag summert (=tykkelse fra topp til bunn av isen)\n')
    outf.write('TI       - Største tykkelse av alle lag summert (=tykkelse fra topp til bunn av isen)\n')
    outf.write('Maksdato    MEL   SØI   STI   TSID        TSI     TID      TI\n')
    fyear = 2020
    lyear = 2020
    # fyear = 1968
    # lyear = 1968
    extremeice = 0
    extremeice2 = 0
    extremeice3 = 0

    # Isgangsdatoer
    # isgangsdatoer = []
    # Beiarn
    # isgangsdatoer = ['13.11.1979', '28.01.1981', '03.12.1982', '07.12.1982', '04.12.1983', '11.05.1985', '16.11.1985',
    #                  '01.05.1986', '03.12.1987', '09.05.1988', '12.11.1988', '04.01.1989', '01.02.1989', '30.11.1989',
    #                  '06.02.1990', '11.04.1990', '02.12.1990', '25.01.1991', '10.12.1992', '01.02.1993', '13.03.1993',
    #                  '08.11.1993', '02.12.1994', '28.11.1995', '28.04.1996']
    # Stjørdalselva
    isgangsdatoer = ['04.04.1969', '10.01.1971', '07.03.1971', '27.11.1971', '14.01.1972', '13.03.1972', '28.11.1972',
                     '13.03.1973', '23.11.1974', '05.02.1975', '19.11.1979', '06.11.1980', '26.01.1981', '11.04.1981',
                     '25.03.1982', '06.02.1990', '23.01.1991', '20.03.1991', '08.02.1993', '11.01.2024']
    for i in range(fyear, lyear + 1):
        fdate = str(i) + '-10-01'
        ldate = str(i + 1) + '-05-31'

        # Her er antatte vanntemperaturer ved gitte lufttemperaturer
        # En array for hver måned med kopier i endene.
        # Dataene er derfor desember, januar, februar, .... , desember, januar. 14 elementer
        # Merk at mai-november er tulledata. Dette er utenfor isleggingen i Numedalslågen
        awt =  [[[-20, 0], [-15, 0], [-10, 0], [-5, 0.5], [0, 1.0], [5, 1.5], [10, 2.0], [15, 2.2], [20, 2.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.1], [5, 0.2], [10, 0.2], [15, 0.3], [20, 0.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.2], [5, 0.3], [10, 0.3], [15, 0.4], [20, 0.5]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.3], [0, 0.8], [5, 1.6], [10, 2.6], [15, 2.8], [20, 3.0]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.4], [0, 1.9], [5, 3.3], [10, 4.5], [15, 5.0], [20, 5.5]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.8], [0, 2.7], [5, 6.0], [10, 7.2], [15, 8.3], [20, 10.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.7], [0, 1.9], [5, 3.3], [10, 4.5], [15, 5.0], [20, 5.5]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.6], [0, 0.8], [5, 1.6], [10, 2.6], [15, 2.8], [20, 3.0]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.5], [0, 1.0], [5, 1.5], [10, 2.0], [15, 2.2], [20, 2.4]], \
                [[-20, 0], [-15, 0], [-10, 0], [-5, 0.1], [0, 0.1], [5, 0.2], [10, 0.2], [15, 0.3], [20, 0.4]]]

        mf = 0.04
        # For Beiarn, ignorer smelting, start potensielt 1. oktober
        # maxice = run_newsite(fdate, ldate, 10, 1, forcing='grid', location_name=location_name,
        #                      plot_folder=se.plot_folder, x=490972, y=7424245, altitude=29,
        #                      icerun_dates=isgangsdatoer)
        # For Stjørdalselva, ignorer smelting, start potensielt 1. oktober
        maxice = run_newsite(fdate, ldate, 10, 1, forcing='grid', location_name=location_name,
                             plot_folder=se.plot_folder, x=316405, y=7042107, altitude=33,
                             icerun_dates=isgangsdatoer)

        #test = get_ice_thickness_given_date(maxice[8][57], maxice[8], maxice[9])
        # altitude = 120
        #mjosice = run_newsite(fdate, ldate, 10, 1, forcing='grid', location_name=location_name,
        #                    plot_folder=se.plot_folder, x=267095, y=6746083, altitude=33,  <- x og y til 32 ikke 33 utm
        #                    icerun_dates=isgangsdatoer)
        outf.write(str(maxice[0])[0:10] + f'{maxice[1]:6.2f}' + f'{maxice[2]:6.2f}' + f'{maxice[3]:6.2f}' + '  '
                   + str(maxice[4])[0:10] + f'{maxice[5]:6.2f}' + '  '
                   + str(maxice[6])[0:10] + f'{maxice[7]:6.2f}' + '\n')
#        print(" icethickness gien date: " + f'{test}' + '\n')
        print("date for thickest single layer: ", maxice[0],
              "\nthickest single layer: ", f'{maxice[1]:6.2f}',
              "\nslush_ice thickness in thickest single layer: ", f'{maxice[2]:6.2f}',
              "\nblack_ice thickness in thickest single layer: ", f'{maxice[3]:6.2f}',
              "\ndate for biggest sum of solid ice layers: ", maxice[4],
              "\nbiggest sum of solid ice layers: ", f'{maxice[5]:6.2f}',
              "\ndate for biggest sum of all ice layers (ice thickness): ", maxice[6],
              "\nbiggest sum of all ice layers (ice thickness): ", f'{maxice[7]:6.2f}')
        if maxice[1] > extremeice:
            extremeice = maxice[1]
            extreme = (maxice[0], maxice[1], maxice[2], maxice[3])
        if maxice[5] > extremeice2:
            extremeice2 = maxice[5]
            extreme2 = (maxice[4], maxice[5])
        if maxice[7] > extremeice3:
            extremeice3 = maxice[7]
            extreme3 = (maxice[6], maxice[7])

        # Save solid ice data
        for k in range(len(maxice[8])):
            outf4.write(maxice[8][k].strftime("%d.%m.%y") + ';' + f'{maxice[9][k]:6.2f}'.replace('.', ',') + '\n')

        # Save air temperature data
        for k in range(len(maxice[10])):
            outf5.write(maxice[10][k].strftime("%d.%m.%y") + ';' + f'{maxice[11][k]:6.1f}'.replace('.', ',') + '\n')

    # Extremes of the extremes
    outf.write('-----------------------------------------------\n')
    outf.write(str(extreme[0])[0:10] + f'{extreme[1]:6.2f}' + f'{extreme[2]:6.2f}' + f'{extreme[3]:6.2f}' + '  '
               + str(extreme2[0])[0:10] + f'{extreme2[1]:6.2f}' + '  '
               + str(extreme3[0])[0:10] + f'{extreme3[1]:6.2f}')

    # print('-----------------------------------------------')
    # print(extreme[0], f'{extreme[1]:6.2f}', f'{extreme[2]:6.2f}', f'{extreme[3]:6.2f}',
    #       extreme2[0], f'{extreme2[1]:6.2f}', extreme3[0], f'{extreme3[1]:6.2f}')
    outf.close()
    outf4.close()
    outf5.close()
    pass
