-- Body of water
CREATE TABLE BodyOfWater (
    Name TEXT PRIMARY KEY
);

-- Single sensor
CREATE TABLE Sensor (
    SensorID INT PRIMARY KEY,
    SensorType TEXT CHECK(SensorType IN ('LiDar', 'Magnetic', 'Other')),
    Active BOOLEAN
);

-- Measurement taken at a given time
CREATE TABLE Measurement (
    MeasurementID INT NOT NULL,
    SensorID INT NOT NULL,
    TimeMeasured DATETIME NOT NULL,
    WaterBodyName TEXT NOT NULL,
    CenterLat FLOAT NOT NULL,
    CenterLon FLOAT NOT NULL,
    FOREIGN KEY (SensorID) REFERENCES Sensor(SensorID),
    FOREIGN KEY (WaterBodyName) REFERENCES BodyOfWater(Name),
    PRIMARY KEY (MeasurementID, WaterBodyName, TimeMeasured)
);

CREATE TABLE SubdivisionMeasurementData (
    MeasurementID INT NOT NULL,
    TimeMeasured DATETIME NOT NULL,
    SubdivID INT NOT NULL,
    WaterBodyName TEXT NOT NULL,
    MinimumThickness FLOAT NOT NULL,
    AverageThickness FLOAT NOT NULL,
    CalculatedSafety FLOAT NOT NULL,
    Accuracy FLOAT,
    FOREIGN KEY (MeasurementID, TimeMeasured, WaterBodyName) REFERENCES Measurement(MeasurementID, TimeMeasured, WaterBodyName),
    PRIMARY KEY (SubdivID, WaterBodyName, TimeMeasured)
);

-- Test data
INSERT INTO Sensor (SensorID, SensorType, Active) VALUES
(1, 'LiDar', 1),
(2, 'Magnetic', 1),
(3, 'LiDar', 0);

INSERT INTO BodyOfWater(Name) VALUES
('Mjøsa'),
('Skumsjøen');

INSERT INTO Measurement (MeasurementID, SensorID, TimeMeasured, WaterBodyName, CenterLon, CenterLat) VALUES
(1, 2, '2024-01-01 10:00:00', 'Mjøsa', 10.1234, 60.4567),
(2, 2, '2024-02-04 11:00:00', 'Mjøsa', 10.2345, 60.5678),
(3, 1, '2024-02-13 12:00:00', 'Mjøsa', 10.3456, 60.6789),
(1, 3, '2024-02-13 12:00:00', 'Skumsjøen', 10.4567, 60.7890),
(2, 3, '2024-02-13 12:00:00', 'Skumsjøen', 10.5678, 60.8901),
(3, 3, '2024-02-13 12:00:00', 'Skumsjøen', 10.6789, 60.9012);


INSERT INTO SubdivisionMeasurementData (MeasurementID, SubdivID, TimeMeasured, WaterBodyName, MinimumThickness,
                                        AverageThickness, CalculatedSafety, Accuracy) VALUES
(1, 1, '2024-01-01 10:00:00', 'Mjøsa', 3.2, 4.5, 2.1, 1.2),
(1, 2, '2024-01-01 10:00:00', 'Mjøsa', 2.8, 4.3, 2.5, 1.1),
(1, 3, '2024-01-01 10:00:00', 'Mjøsa', 4.1, 5.7, 2.3, 1.3),
(1, 4, '2024-01-01 10:00:00', 'Mjøsa', 3.5, 5.0, 2.2, 1.4),
(2, 1, '2024-02-04 11:00:00', 'Mjøsa', 4.1, 5.7, 2.3, 1.3),
(2, 2, '2024-02-04 11:00:00', 'Mjøsa', 3.5, 5.0, 2.2, 1.4),
(3, 1, '2024-02-13 12:00:00', 'Mjøsa', 4.1, 5.7, 2.3, 1.3),
(3, 2, '2024-02-13 12:00:00', 'Mjøsa', 3.5, 5.0, 2.2, 1.4),
(1, 1, '2024-01-01 10:00:00', 'Skumsjøen', 3.8, 5.3, 2.2, 1.2),
(1, 2, '2024-01-01 10:00:00', 'Skumsjøen', 2.4, 4.8, 2.3, 1.1),
(1, 3, '2024-01-01 10:00:00', 'Skumsjøen', 4.5, 6.1, 2.5, 1.3),
(2, 1, '2024-02-04 11:00:00', 'Skumsjøen', 4.5, 6.1, 2.5, 1.3),
(2, 2, '2024-02-04 11:00:00', 'Skumsjøen', 3.9, 5.6, 2.3, 1.4),
(3, 1, '2024-02-13 12:00:00', 'Skumsjøen', 3.8, 5.3, 2.2, 1.2),
(3, 2, '2024-02-13 12:00:00', 'Skumsjøen', 2.4, 4.8, 2.3, 1.1);
