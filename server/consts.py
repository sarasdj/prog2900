# consts.py

# Server variables
HOST = "0.0.0.0"
PORT = 8443

# Certificate paths
CERT_DIR = "server/certificates/"
SSL_KEY_PATH = CERT_DIR + "testKey.key"
SSL_CERT_PATH = CERT_DIR + "testCert.crt"

# File paths
MAP_HANDLER_PATH = "server/map_handler/"
LAKE_RELATIONS_PATH = MAP_HANDLER_PATH + "lake_relations/"
LIDAR_DATA_PATH = "server/lidar_data/"
STATS_OUTPUT_PATH = "server/ModelFromNVE/outputs/plots/"
