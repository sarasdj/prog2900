import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import '../../consts.dart';


// Saves all measurements to a file on the users mobile device
Future<void> _exportIceData() async {
  final directory = await getExternalStorageDirectory();
  final file = File('${directory?.path}/ice_data_$selectedLake.json');

  List<Map<String, dynamic>> measurementsJSON = [];
  // Convert every measurement to JSON
  for (var element in selectedMeasurements) {
    measurementsJSON.add(element.toJson());
  }

  // Write JSON data to file
  await file.writeAsString(measurementsJSON.toString());
}


// Display a progress indicator while JSON data is being downloaded
void showProgressIndicator(BuildContext context) {
  BuildContext? dialogContext;

  showDialog(
    context: context,
    builder: (BuildContext context) {
      dialogContext = context;
      return WillPopScope(
        onWillPop: () async => false, // Prevent dialog from being closed by user
        child: const AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CircularProgressIndicator(), // Progress indicator
              SizedBox(height: 20),
              Text('Exporting JSON data...'),
            ],
          ),
        ),
      );
    },
  );

  // Ensure that the progress indicator runs for at lest 1 second
  Future.delayed(const Duration(seconds: 1), () {
    try { // Download JSON data
      _exportIceData();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Downloaded ice data as JSON')),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Failed to export JSON data: $e')),
      );
    } finally {
      if (dialogContext != null) {
        // Add 2 second delay before closing dialog
        Future.delayed(const Duration(seconds: 2), () {
          Navigator.of(dialogContext!).pop();
          Navigator.of(context).pop();
        });
      }
    }
  });
}

