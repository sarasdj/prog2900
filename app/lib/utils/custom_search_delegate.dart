import 'package:fuzzy/fuzzy.dart';
import 'package:flutter/material.dart';

import '../consts.dart';

typedef SearchResultCallback = void Function(String result);

class CustomSearchDelegate extends SearchDelegate {
  final SearchResultCallback onResultSelected;

  CustomSearchDelegate(this.onResultSelected);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton( // Clear query
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton( // Close search bar
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<String> searchResults = [];
    final options = FuzzyOptions(threshold: 0.4, findAllMatches: true);
    final matcher = Fuzzy(lakeSearchOptions, options: options);
    final results = matcher.search(query);
    searchResults = results.map((result) => result.item as String).toList();

    return ListView.builder(
      itemCount: searchResults.length,
      itemBuilder: (context, index) {
        var result = searchResults[index];
        return ListTile(
          title: Text(result),
          tileColor: Colors.red,
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> searchResults = [];
    final options = FuzzyOptions(threshold: 0.4, findAllMatches: true);
    final matcher = Fuzzy(lakeSearchOptions, options: options);
    final results = matcher.search(query);
    searchResults = results.map((result) => result.item as String).toList();

    return ListView.builder(
      itemCount: searchResults.length,
      itemBuilder: (context, index) {
        var result = searchResults[index];
        if  (searchResults.isNotEmpty) {
          return ListTile(
            title: Text(result),
            onTap: () {
              onResultSelected(result);
              close(context, result);
            },
          );
        }
        else {
          return const ListTile(
            title: Text("No matches found"),
          );
        }
      },
    );
  }
}