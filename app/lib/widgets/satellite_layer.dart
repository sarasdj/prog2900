import 'package:latlong2/latlong.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

import '../data_classes.dart';

class SatelliteLayer extends StatelessWidget {
  final List<Measurement> markerList;

  const SatelliteLayer({
    Key? key,
    required this.markerList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        center: LatLng(60.7666, 10.8471),
        zoom: 9.0,
      ),
      children: [
        TileLayer( // Map from OpenStreetMap
          urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
          subdomains: const ['a', 'b', 'c'],
        ),
      ],
    );
  }
}
