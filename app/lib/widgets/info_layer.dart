import 'package:flutter/material.dart';

import '../../consts.dart';

class InfoLayer extends StatefulWidget {

  const InfoLayer({
    Key? key,
  }) : super(key: key);

  @override
  InfoLayerState createState() => InfoLayerState();
}

class InfoLayerState extends State<InfoLayer> {
  @override
  void initState() {
    super.initState();
  }

  // _buildLegendItem renders a colored circle and text to form a legend
  Widget _legendItem(Color color, String text) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 20,
          decoration: BoxDecoration(
            color: color,
            shape: BoxShape.circle,
          ),
        ),
        const SizedBox(width: 8),
        Text(
          text,
          style: const TextStyle(
            fontSize: 14,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  /// Builds an additional legend to explain the colors
  Widget _buildLegend() {
    return Column(
      children: [
        _legendItem(const Color(0xffff0000), "Certainly"),
        const SizedBox(height: 10),
        _legendItem(const Color(0xffff6a00), "Very likely"),
        const SizedBox(height: 10),
        _legendItem(const Color(0xFFb1ff00), "Very unlikely"),
        const SizedBox(height: 10),
        _legendItem(const Color(0xFF00d6ff), "Extremely unlikely"),
        const SizedBox(height: 10),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(45),
              color: Colors.black.withOpacity(0.85),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center, // Align contents vertically centered
                children: [
                  Text(
                    'Color categorization',
                    style: subHeadingStyle,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Each category shows the likelihood of the ice breaking '
                        'under the weight of a single person.',
                    style: regTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 30),
                  _buildLegend(),
                  const SizedBox(height: 30),
                  Text(
                    'Data accuracy',
                    style: subHeadingStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'The higher the accuracy, the more likely that the displayed ice thickness'
                        'is correct. The accuracy depends on'
                        ' which data sources are utilized in the determination of the thickness. ',
                    style: regTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20),
                  Text('• 1/4: only API data. \n'
                        '• 2/4: only sensor data.\n'
                        '• 3/4: both API and sensor data. \n'
                        '• 4/4: both API and sensor data,  with a discrepancy of <1.0cm',
                    style: regTextStyle,
                  ),
                  const SizedBox(height: 80),
                ],
              ),
            ),
          ),
          Positioned( // Gradient at the bottom of the box
            bottom: 0,
            left: 0,
            right: 0,
            child: IgnorePointer(
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.black.withOpacity(0.0),
                      Colors.black.withOpacity(0.8),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
    );
  }
}
