import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'bar_data.dart';
import '../../consts.dart';

class StatCharts extends StatelessWidget {
  const StatCharts({Key? key}) : super(key: key);

  Widget buildLineChart(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 20),
        child: LineChart(
          LineChartData(
            backgroundColor: Colors.grey.shade800,
            titlesData: FlTitlesData(
              leftTitles: SideTitles(
                showTitles: true,
                reservedSize: 22,
                interval: 10,
                getTextStyles: (value) => const TextStyle(color: Colors.white60),
              ),
              bottomTitles: SideTitles(
                showTitles: true,
                reservedSize: 22,
                getTextStyles: (value) => const TextStyle(color: Colors.white60),
                getTitles: (double value) {
                  switch (value.toInt()) { // Map int values to months
                    case 0:
                      return 'January';
                    case 1:
                      return 'February';
                    case 2:
                      return 'March';
                    case 3:
                      return 'April';
                    default:
                      return '...';
                  }
                },
              ),
            ),
            borderData: FlBorderData(
              show: true,
              border: Border.all(color: Colors.white, width: 1), // Set border properties
            ),
            minX: 0,
            maxX: 4,
            minY: 0,
            maxY: 50,
            lineBarsData: [
              LineChartBarData(
                spots: [
                  FlSpot(0, 34),
                  FlSpot(1, 22),
                  FlSpot(2, 30),
                  FlSpot(3, 15),
                  FlSpot(4, 5),
                ],
                isCurved: true,
                colors: [Colors.blue.shade200],
              ),
            ],
          ),
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          child: Center(
            child: Text(
              'Ice layers (cm)',
              style: regTextStyleBig,
            ),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width-30,
          child: const BarData(),
        ),
        /*
        const SizedBox(height: 60),
        SizedBox(
          child: Center(
            child: Text(
              'Total ice thickness',
              style: regTextStyleBig,
            ),
          ),
        ),
        const SizedBox(height: 10),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.8, // Set the width of the LineChart
          height: 200,
          child: buildLineChart(context),
        ),*/
      ],
    );
  }
}

