import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_map/flutter_map.dart';

import '../data_classes.dart';

class OSM extends StatefulWidget {
  final List<Measurement> markerList;

  const OSM({
    Key? key,
    required this.markerList,
  }) : super(key: key);

  @override
  OSMState createState() => OSMState();
}

class OSMState extends State<OSM> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        bounds: LatLngBounds(
          LatLng(61.1947, 10.3491),
          LatLng(60.3592, 11.3805),
        ),
      ),
      children: [
        TileLayer(
          urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
          subdomains: const ['a', 'b', 'c'],
        ),
      ],
    );
  }
}
