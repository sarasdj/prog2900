import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

class QuickViewChart extends StatelessWidget {
  const QuickViewChart({super.key});

  @override
  Widget build(BuildContext context) {
    return LineChart(
      LineChartData(
        titlesData: FlTitlesData(
          leftTitles: SideTitles(showTitles: true),
          bottomTitles: SideTitles(showTitles: true),
        ),
        borderData: FlBorderData(
          show: true,
        ),
        minX: 0,  // Test data
        maxX: 4,
        minY: 0,
        maxY: 50,
        lineBarsData: [
          LineChartBarData(
            spots: [
              FlSpot(0, 10),  // Test data
              FlSpot(1, 20),
              FlSpot(2, 30),
              FlSpot(3, 40),
            ],
            isCurved: true,
            colors: [Colors.blue],
          ),
        ],
      ),
    );
  }
}
