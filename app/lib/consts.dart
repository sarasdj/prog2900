import 'dart:typed_data';
import 'package:app/data_classes.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// API variables
const String port = "8443";
const String serverURI = "https://127.0.0.1:$port/";
const String mapEndpoint = "update_map";

// Map variables
String selectedLake = 'Mjøsa';
Uint8List selectedRelation = Uint8List(0); // Initialised in init_state.dart
List<Measurement> selectedMeasurements = [];
List<SubDiv> selectedSubdivisions = [];
SubDiv? selectedSubDiv;

DateTime ?lastUpdate; // Last time data was fetched from server
List<String> lakeSearchOptions = [];
bool internetConnection = true;

// Font settings
const textColor = Colors.white;
final appTitleStyle = GoogleFonts.chakraPetch(
  fontSize: 35,
  color: Colors.black,
  fontWeight: FontWeight.bold,
);
final titleStyle = GoogleFonts.chakraPetch(
  fontSize: 30,
  color: Colors.white70,
  fontWeight: FontWeight.bold,
);

final smallTextStyle = GoogleFonts.chakraPetch(fontSize: 13, color: textColor);
final regTextStyle = GoogleFonts.chakraPetch(fontSize: 16, color: textColor);
final regTextStyleBig = GoogleFonts.chakraPetch(fontSize: 20, color: textColor);
final chartTextStyle = GoogleFonts.chakraPetch(fontSize: 12, color: textColor);
final subHeadingStyle = GoogleFonts.chakraPetch(fontSize: 18, color: textColor, fontWeight: FontWeight.bold);
