import 'dart:async';
import 'package:flutter/material.dart';
import 'package:app/server_requests/init_state.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

import '../consts.dart';
import '../widgets/main_layout.dart';
import '../utils/custom_search_delegate.dart';

class DefaultPage extends StatefulWidget {
  const DefaultPage({Key? key}) : super(key: key);

  @override
  _DefaultPageState createState() => _DefaultPageState();
}

class _DefaultPageState extends State<DefaultPage> {
  late Timer _timer;
  bool serverConnection = true;
  bool dialogShown = false;
  final backgroundColor = Colors.black87;

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> _handleRefresh() async {
    return await initialiseState(false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: backgroundColor,
        appBar: AppBar(
          backgroundColor: backgroundColor,
          leading: IconButton(
            icon: const Icon(
                Icons.menu,
                color: Colors.white54
            ),
            onPressed: () {
              // Not implemented
            },
          ),
          title: Text(
            selectedLake, // Display name of current map
            style: regTextStyleBig,
          ),
          actions: [
            IconButton(
              icon: const Icon(
                  Icons.search,
                  color: Colors.white54
              ),
              onPressed: () {
                showSearch( // Fetch new relation and measurements on search
                  context: context,
                  delegate: CustomSearchDelegate((String result) {
                    // Make request only if the selected lake is different from the current selected lake
                    if (result != selectedLake) {
                      setState(() {
                        selectedLake = result;
                        initialiseState(false);
                      });
                    }
                  }),
                );
              },
            ),
          ],
        ),
        body: LiquidPullToRefresh(
          color: backgroundColor,
          height: 100,
          backgroundColor: Colors.grey[600],
          onRefresh: _handleRefresh,
          animSpeedFactor: 3,
          showChildOpacityTransition: false,
          child: Container( // Return main container with map and stats widget
            color: const Color(0xff151515),
            child: ListView(
              children: [
                MapContainerWidget(
                  subdivisions: selectedSubdivisions,
                  measurements: selectedMeasurements,
                  relation: selectedRelation,
                  serverConnection: serverConnection,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void fetchNewLake(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false, // Prevent dismissal by user
    builder: (BuildContext dialogContext) {

      bool initialized = false;

      // Display CircularProgressIndicator
      Future.delayed(const Duration(milliseconds: 500), () {
        if (!initialized) {
          showDialog(
            context: dialogContext,
            builder: (BuildContext _) => const Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      });

      initialiseState(false).then((_) {
        // Mark initialization as complete
        initialized = true;

        Navigator.of(dialogContext, rootNavigator: true).pop();
      });

      // Return a placeholder widget for the dialog
      return const SizedBox.shrink();
    },
  );
}

