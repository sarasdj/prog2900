import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'default_page.dart';
import '../server_requests/init_state.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({super.key});

  @override
  State<StatefulWidget> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage>
    with SingleTickerProviderStateMixin{

  @override
  void initState() {
    super.initState();
    // Remove app bar
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);

    // Call initialiseState() asynchronously and wait for it to complete
    _navigateToDefaultPage();
  }

  Future<void> _navigateToDefaultPage() async {
    await initialiseState(true);

    // Navigate to the default page once state is initialised
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (_) => const DefaultPage(),
    ));
  }

  @override
  void dispose() { // Add back app bar
    super.dispose();
    SystemChrome.setEnabledSystemUIMode(
        SystemUiMode.manual,
        overlays: SystemUiOverlay.values,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.grey.shade900,
        ),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/icons/frozen.png',
                // Icon from: https://www.flaticon.com/free-icons/cold-water"
                color: Colors.grey,
                height: 300,
                width: 300,
              ),
              const SizedBox(height: 20),
              const Text(
                "IceMap",
                style: TextStyle(
                    color: Colors.white70,
                    fontStyle: FontStyle.italic,
                    fontSize: 30,
                ),
              )
          ]
        ),
      ),
    );
  }
}