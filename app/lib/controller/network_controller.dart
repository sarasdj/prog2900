import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../consts.dart';

/// NetworkController checks the network connection of the application globally
class NetworkController extends GetxController {
  final Connectivity _connectivity = Connectivity();

  @override
  void onInit() {
    super.onInit();
    _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  void _updateConnectionStatus(ConnectivityResult connectivityResult) {

    // If no network connection, show snack-bar
    if (connectivityResult == ConnectivityResult.none) {
      internetConnection = false;

      Get.rawSnackbar(
        messageText: Text(
          'No internet connection. The displayed information may be outdated!',
            style: regTextStyle,
        ),
        isDismissible: false,
        duration: const Duration(days: 1), // Display the message until a network connection is established
        backgroundColor: Colors.black45,
        icon: const Icon(
          Icons.wifi_off,
          color: Colors.white,
          size: 35,
        ),
        margin: EdgeInsets.zero,
        snackStyle: SnackStyle.GROUNDED,
      );
    } else {
      internetConnection = true;
      
      if (Get.isSnackbarOpen) { // Close snack-bar upon establishing internet connection
        Get.closeCurrentSnackbar();
      }
    }
  }
}