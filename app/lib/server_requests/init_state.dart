import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:app/consts.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data_classes.dart';
import '../server_requests/fetch_measurements.dart';
import '../server_requests/fetch_relation.dart';

/// initialiseState makes three requests to the server, one requesting
/// measurements for the selected relation, the other requesting the relation,
/// and the last requesting the list of all system lakes
Future<void> initialiseState(bool initNewLake) async {
  bool serverConnection = true;

  late Future<List<Measurement>> markerListFuture;
  late Future<Uint8List> relationFuture;

  try {
    if (!internetConnection) {  // Read data from files if no internet connection
      selectedRelation = await loadRelation();

      FetchResult fetchResult = await loadMeasurements();
      List<Measurement> measurements = fetchResult.measurements;
      selectedMeasurements = measurements;

      // Extract all _subdivisions from list of measurements
      for (Measurement measurement in measurements) {
        for (SubDiv subdivision in measurement.subDivs) {
          selectedSubdivisions.add(subdivision);
        }
      }

      // Sort the list of SubDiv objects based on each subdivision id
      selectedSubdivisions.sort((a, b) => a.sub_div_id.compareTo(b.sub_div_id));

      print("Loaded from files: Meas.len: ${selectedMeasurements.length}, rel.len: ${selectedRelation.length}");

    } else { // Try to fetch measurement data from server
      markerListFuture = fetchMeasurements().then((fetchResult) {
        List<Measurement> measurements = fetchResult.measurements;
        selectedMeasurements = measurements;

        // Extract all _subdivisions from list of measurements
        for (Measurement measurement in measurements) {
          for (SubDiv subdivision in measurement.subDivs) {
            selectedSubdivisions.add(subdivision);
          }
        }

        // Sort the list of SubDiv objects based on each subdivision id
        selectedSubdivisions.sort((a, b) => int.parse(a.sub_div_id).compareTo(int.parse(b.sub_div_id)));

        serverConnection = fetchResult.connected;
        setLastLake(); // Update persistent value for latest fetched lake

        // Return measurements
        return measurements;
      }).catchError((error) {
        serverConnection = false;
        throw Exception("Failed to fetch measurements: $error");
      });

      // If measurements were fetched successfully, request relation
      if (serverConnection) {
        relationFuture = fetchRelation();
      } else { // Read last saved data
        relationFuture = loadRelation();
      }

      if (initNewLake) {
        // Last lake initialised to last persistent variable, or Mjøsa if the variable is not found
        final prefs = await SharedPreferences.getInstance();
        selectedLake = prefs.getString('lastLake') ?? "Mjøsa";

        initSearchOptions();
      }

      // Set the selected relation
      selectedRelation = await relationFuture;
      selectedMeasurements = await markerListFuture;
    }
  } catch (e) {
    // Handle any errors that occur during the initialization process
    print("Error during initialization: $e");
  }
}


/// initSearchOptions fetches a list of all lake names in the system
/// and initialises lakeSearchOptions
Future<void> initSearchOptions() async {
  try {
    HttpClient client = HttpClient()
      ..badCertificateCallback = // NB: temporary disable SSL certificate validation
          (X509Certificate cert, String host, int port) => true;

    var request = await client.getUrl(Uri.parse('${serverURI}get_lake_names'));
    var response = await request.close();

    if (response.statusCode == 200) {
      var responseBody = await response.transform(utf8.decoder).join();

      if (responseBody.isNotEmpty) {
        var jsonData = json.decode(responseBody);

        if (jsonData != null && jsonData is List) {
          lakeSearchOptions = jsonData.map<String>((item) => item.toString()).toList();
          return;
        }
      }
    }
  } catch (e) {
    print("Failed to fetch lake names: $e");
  }
}

Future<void> setLastLake() async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.setString('lastLake', selectedLake);
}
