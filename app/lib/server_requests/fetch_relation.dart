import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';

import '../../consts.dart';

/// Fetch relation data from server
Future<Uint8List> fetchRelation() async {
  try {
    // Custom HTTP client
    HttpClient client = HttpClient()
      ..badCertificateCallback = // NB: temporary disable SSL certificate validation
          (X509Certificate cert, String host, int port) => true;

    // Execute request to to get_relation endpoint
    var request = await client.getUrl(Uri.parse('${serverURI}get_relation?lake='
        '${Uri.encodeFull(selectedLake)}'));

    var response = await request.close(); // Close response body at end of function

    // Try to parse body to JSON if request is ok
    if (response.statusCode == 200) {
      var responseBody = await response.transform(utf8.decoder).join();

      if (responseBody.isNotEmpty) {
        Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
        String filePath = '${appDocumentsDirectory.path}/last_relation.json';

        try { // Write most recent time of update to file
          await File(filePath).writeAsString(responseBody, mode: FileMode.write);
          print('Relation written to file');
        } catch (error) { print('Error in writing to file: $error');}

        // Return relation data from the response body
        return Uint8List.fromList(utf8.encode(responseBody));
      }
    }
    return loadRelation();
  } catch (e) {
    print("Error in fetching relation from server: $e");
    return loadRelation();
  }
}

/// Load last saved relation data form last_relation.json
Future<Uint8List> loadRelation() async {
  try {
    print("Loading relation from file");
    // Get latest saved relation from file if the server does not respond
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
    String filePath = '${appDocumentsDirectory.path}/last_relation.json';

    // Read file contents as bytes
    File file = File(filePath);
    if (await file.exists()) {
      Uint8List bytes = await file.readAsBytes();
      return bytes;
    } else {
      throw Exception('Relation file does not exist');
    }
  } catch (error) {
    print('Error in reading relation from file: $error');
    return Uint8List(0);
  }
}
