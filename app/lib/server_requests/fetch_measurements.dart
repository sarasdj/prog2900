import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../consts.dart';
import '../data_classes.dart';

class FetchResult {
  final List<Measurement> measurements;
  final bool connected;

  FetchResult(this.measurements, this.connected);
}

/// fetchMarkerData fetches measurement data from the server
Future<FetchResult> fetchMeasurements() async {
  try {
    // Custom HTTP client
    HttpClient client = HttpClient()
      ..badCertificateCallback = // NB: temporary disable SSL certificate validation
          (X509Certificate cert, String host, int port) => true;

    // Request markers from server
    var request = await client.getUrl(Uri.parse('$serverURI$mapEndpoint?lake='
        '${Uri.encodeFull(selectedLake)}'));

    var response = await request.close(); // Close response body at end of function

    // Parse body to JSON if request is ok
    if (response.statusCode == 200) {
      var responseBody = await response.transform(utf8.decoder).join();

      if (responseBody.isNotEmpty) {
        var jsonData = json.decode(responseBody);

        // Attempt to parse response to Measurement object only if the body
        // contains correctly formatted data
        if (jsonData != null && jsonData is List) {
          Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
          String filePath = '${appDocumentsDirectory.path}/last_data.json';

          try { // Write most recent time of update to file
            await File(filePath).writeAsString(responseBody, mode: FileMode.write);
            print('Lake data written to file');
          } catch (error) { print('Error in writing to file: $error');}

          // Update local and persistent lastUpdate variable
          lastUpdate = DateTime.now();
          final prefs = await SharedPreferences.getInstance();
          await prefs.setString('lastUpdate', '${DateTime.now()}');

          return FetchResult(jsonData.map((data) => Measurement.fromJson(data)).toList(), true);
        }
      }
    }
    return loadMeasurements();
  } catch (e) {
    print("Error in fetching measurements from server: $e");
    return loadMeasurements();
  }
}

Future<FetchResult> loadMeasurements() async {
  try {
    print("Loading measurements from file");
    // Get latest saved data from file if the server does not respond
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
    String filePath = '${appDocumentsDirectory.path}/last_data.json';

    // Read file contents
    File file = File(filePath);
    if (await file.exists()) {

      print('Reading marker data from file');

      String contents = await file.readAsString();
      List<dynamic> jsonData = json.decode(contents); // Parse JSON string from file
      List<Measurement> measurements = jsonData.map((data) => Measurement.fromJson(data)).toList();
      return FetchResult(measurements, false);
    } else {
      throw Exception('File does not exist');
    }
  } catch (error) {
      print('Error in reading measurements from file: $error');
      return FetchResult([], false);
  }
}
