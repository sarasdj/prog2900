import 'package:get/get.dart';
import 'package:flutter/material.dart';

import 'pages/loading_page.dart';
import 'package:app/controller/dependency_injection.dart';

void main() {
  runApp(const IceMap());
  DependencyInjection.init();
}

class IceMap extends StatelessWidget {
  const IceMap({super.key});

  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp( // GetMaterialApp for snack-bar support
      home: LoadingPage(),
    );
  }
}
