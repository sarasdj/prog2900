// src: https://github.com/garmin/LIDARLite_Arduino_Library/tree/3.0.6
// gps code: https://projecthub.arduino.cc/the_electro_artist/arduino-location-tracker-ed1f85
// must have NEO-6M for the gps

#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <LIDARLite.h>
#include <SD.h>
#include <SPI.h>

LIDARLite myLidarLite;  // LiDar object
TinyGPSPlus gps;        // GPS object
SoftwareSerial ss(RXPin, TXPin); // serial connection to GPS device

// structrue of each measurement in waterbody
struct WaterBody {
  int measurementID;
  const char* name;
  float latitude;
  float longitude;
};

// NB: if new coordinates or body of water is added then add it here
// define an array of waterbody structures
WaterBody waterCoords = {
  {"mjosa",60.000000,12.000000, 1},
  {"mjosa",65.000000,11.000000, 2},
  {"mjosa",59.000000,12.000000, 3},
  {"mjosa",63.000000,13.000000, 4},
};

#define CHIP_SELECT_PIN 10 \\ Pin connected to the CS pin of SD card moudle
#define MAX_READING 1000 \\ Maximum number of LIDAR readings to store
#define RXPIN 2
#define TXPIN 3

static const uint32_t GPSBaoud = 9600; // baud rate
const float maxRadius = 5; // max distance from location to gps coordinates 

void setup() {
    Serial.begin(GPSBaud); // Initialize serial connection to display distances readings
    pinMode(CHIP_SELECT_PIN, OUTPUT);

    if (!SD.begin(CHIP_SELECT_PIN)){
      Serial.println("SD card initialization failed");
      return;
    }
    
    Serial.println("SD card initialized");

    myLidarLite.begin(0, true); // Start sensor and I2C
    myLidarLite.configure(0); // set to default mode, balanced performance
    ss.begin(GPSBaud); // Start GPS tracking

}

// extra: Reciever bias correction is performed 1 out of every 100 readings
void loop() {

  while (ss.available() > 0){
    gps.encode(ss.read());
    if (gps.location.isUpdated()){ 
      for (int i = 0; i< sizeof(waterCoords)/sizeof(waterCoords[0]); i++){
        // scan after lidar reached designated location
        if withinRadius(gps.location.lat, gps.location.lng, waterCoords[i].latitude, waterCoords[i].longitude, maxRadius) {
          String filename = String(waterCoords[i].name) + "_ID_" + String(waterCoords[i].measurementID) + ".txt";
          
          if (SD.exist(filename)){
            SD.remove(filename);
            Serial.println("File deleted:" + filename);
          }

          // Create a new file on the SD card
          File dataFile = SD.open(filename, FILE_WRITE);

          if (!dataFile){
            Serial.println("SD card initialized");
          
            // Take 99 measurement without receiver bias correction and print to file
            for(int i = 0; i < 10; i++){
              // convert it to (XYZ) format
              sprintf(coordinate,"%d %d %d\n\0",(int)(0,0,myLidarLite.distance(false));
              datafile.println(coordinate);
            }

            dataFile.close();
            // Convert and send them
            Serial.print(s);
            Serial.println("File created.", filename);
          }
          else{
            Serial.println("Error opening file for writing: ", filename);
          }
        }
      }
    }
  }
}

// Take measurement 
// param bias - with or without reciver bias correction
void receive(bool bias)
{
  Serial.println(myLidarLite.distance(bias));
}

// check if lidar is close enough for a scan
bool withinRadius(float x1,float y1,float x2,float y2, float radius){
  float distance = sqrt(pow((x1-x2),2)+pow((y1-y2),2));
  return (distance <= radius);
}