# IceMap PROG2900
This software was developed at NTNU as part of a Bachelor thesis for the municipality of Gjøvik.

## Table of contents
* [IceMap sofwtare license](#icemap-software-license)
* [Server](#server)
  * [Endpoints](#endpoints)
    * [Update map](#update-map)
    * [Get relation](#get-relation)
    * [Get lake names](#get-lake-names)
    * [Update measurements](#update-measurements)
    * [Add new lake](#add-new-lake)
    * [Add test data](#add-test-data)
    * [New lidar data](#new-lidar-data)
  * [Database](#database)
  * [Server dependencies](#server-dependencies)
  * [Known bugs](#known-bugs)
* [Application](#application)
  * [Application dependencies](#application-dependencies)
* [Contributors](#contributors)
  * [Developers](#developers)
  * [Third party software](#third-party-software)

# IceMap software license
The IceMap software was produced by Joakim Aleksandersen, 
Sara Savanovic Djordjevic, and Hoa Ben The Nguyen.

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation to view the Software. 
Any distribution and modification of the Software requires prior written 
permission from the developers of the software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# Server
The server currently only accepts HTTPS requests, not HTTP.
It listens on ip address 127.0.0.1 and port 8443. 

URL: ```https://127.0.0.1:8443```

## Endpoints
This server consists of 7 endpoints. Each endpoint requires the desired lake name
to be provided as an url parameter. Of the 7 endpoints, the first 5 only accept GET requests, while
```new_lidar_data``` only accepts POST requests. The first three endpoints expose the processed
data to the application.

Application endpoints:
```
.../update_map?lake=*
.../get_relation?lake=*
.../get_lake_names

```

Other endpoints:
```
.../update_measurements?lake=*
.../add_new_lake?lake=*&cell_size=*
.../add_test_data?lake=*
.../new_lidar_data?lake=*
```

Request examples:
```
https://127.0.0.1:8443/update_map?lake=Mjøsa
https://127.0.0.1:8443/get_relation?lake=Mjøsa
https://127.0.0.1:8443/get_lake_names

https://127.0.0.1:8443/update_measurements?lake=Mjøsa
https://127.0.0.1:8443/add_new_lake?lake=Skumsjøen&cell_size=0.8
https://127.0.0.1:8443/add_test_data?lake=Mjøsa
https://127.0.0.1:8443/new_lidar_data?lake=Mjøsa
```

### Update map
```
Method: GET
Path: update_map?lake=*
Paramters: 
    - lake (required)
```

Returns the contents of the measurement file for the selected lake. 
The file which is read is called ```*_measurements.json``` and can be found in 
```server/map_handler/lake_relations```. This endpoint makes no changes to any files.
This endpoint exposes data for the application.

### Get relation
```
Method: GET
Path: get_relation?lake=*
Paramters: 
    - lake (required)
```

Returns the contents of the relation file for the selected lake.
The file which is read is called ```*_div.json``` and can be found in
```server/map_handler/lake_relations```. This endpoint makes no changes to any files.
This endpoint exposes data for the application.

### Get lake names
```
Method: GET
Path: get_lake_names
Paramters: none
```

```get_lake_names``` returns the contents of the file ```all_lake_names.json```. The file
contains the names of all the lakes in the system. This list is utilized by the applications search
functionality. This endpoint makes no changes to any files.

### Update measurements
```
Method: GET
Path: update_measurements?lake=*
Paramters: 
    - lake (required)
```

Updates the ```*_measurements.json``` for the selected lake.
The endpoint will retrieve the latest LiDar data from ```_lidar_data.json```, which
is also located in ```map_handler/lake_relations/```. The endpoint will also update the
ice statistics form the NVE model. This endpoint exposes data for the application.

### Add new lake
```
Method: GET
Path: add_new_lake?lake=*&cell_size=*
Paramters: 
    - lake (required)
    - cell_size (optional)
```

Is used when a new lake is added to the system. This process
requires manual intervention. To add a new lake to the system, first navigate to 
https://overpass-turbo.eu/.  Once you have navigated to th OverpassTurbo API, copy the
query below, and enter it into the white field at the left-hand side at the website
. Swap 'lakeName' out with the name of the lake you want to add. Once the query has been 
adjusted, press the 'Run' button.

```
[out:json];
(
  way["natural"="water"]["name"="lakeName"];
  relation["natural"="water"]["name"="lakeName"];
);
(._;>;);
out body;
```

![img.png](images/overpass-query.png)
![img.png](images/geojson-export.png)

If a text box saying "This query returned quite a lot of data (approx. x MB). Your browser may have a hard time trying to render this. Do you really want to continue?
" appears, press 'continue anyway'. Double check that you have
the correct lake, then press 'Export'. In the 'Export' menu, download the shape data as
GeoJson. Once downloaded, name the file the ```*lakeName.json```, and move the file into
```/server/map_handler/lake_relations```. Once you have added the file, make a get request to the endpoint.
The endpoint requires that the lake name is provided as a parameter. The ```cell_size``` determines
the dimensions of the subdivisions in kilometers. This parameter is optional, but if a value is not provided
in the requests, the default size will be 0.5km.

![img.png](images/geojson-file.png)

The endpoint will create the file ```lakeName_div.json```. The original GeoJSON file from OverpassTurbo
file should also remain in the system. Do not remove this file. Additionally, the file named ```all_lake_names.json```
should be updated to contain the newly added lake name.

![img.png](images/resulting-files.png)


#### Adding an aree for ice run dates
When adding a lake, a bounding box should also be added inside the ```/server/Sentinelhub/box.json``` file. To view and add new boxes copy and paste the current boxes into sentinel-hub.com/eo-browser, then mark a new area. Make sure the are covers an sufficient area (at least 1 km^2).

![img.png](images/sentinelhub_adding_bboxes.png)
![img.png](images/selecting-bbox.png)

When updating the ice run dates, the corresponding files will be automatically created.
This is necessary to do when creating a new lake, but can also be to add more boxes for large lakes. 


### Add test data
```
Method: GET
Path: add_test_data?lake=*
Paramters: 
    - lake (required)
```
As the name implies, this endpoint adds test data to the desired lake. The endpoint will
overwrite the file ```*_lidar_data.json``` with measurement objects with random thicknesses. 
This endpoint is only for production, and is not implemented in the application.

### New lidar data
```
Method: POST
Path: new_lidar_data?lake=*
Paramters: 
    - lake (required)
```
The endpoint will update the current data from ```*_lidar_data.json```, 
with the content from folder ```./server/lidar_data/*```. Should be used after the 
```./server/lidar_data/*``` has been updated. 

### adding new measurement position in a body of water
```
Method: POST
Path: add_new_lidar_measurement?lake=*&latitude=*&longitude=*
Paramters: 
    - lake (required)
    - latitude (required)
    - longitude (required)
```
The endpoint will add a new measurement positions in a body of water. By 
adding a new measurement to ```*_lidar_data.json``` file. 

## Database
This project requires SQLite3. Download the precompiled binary for your operating system. 
Precompiled binaries can be found on https://www.sqlite.org/download.html. Extract the downloaded
binary in a folder and note its path. Add the path to your system environment variables. Now you can 
manage the SQLite database.

## Server dependencies

## Known bugs
```add_new_lake.py``` does all the processing of new lake data. The current implementation of the
shape file processing fails to include internal coordinates. This results in fully enclosed shapes, such
as islands, to not be included. Simply including internal coordinates without further accommodations will cause
the server to throw errors.

# Application
In order to run the application in an IDE like Android Studio, the port randomization of Dart must
first be bypassed. To do this, run the adb command ```reverse tcp:8443 tcp:8443 ```. This must be
repeated every time the IDE in which the application is running is restarted. A simpler alternative
is to add the command to the run configuration as an external tool. If the port randomization
is not bypassed, the application will not be able to communicate with the server.

## Application dependencies
The application depends on multiple external libraries. A list of these can be found in the
pubspec.yaml file, each with a comment explaining the dependencies usage. 
To install all the dependencies, simply run ```flutter pub get``` inside the app directory.


# Contributors
## Developers
This software was developed by Joakim Aleksandersen, Sara Savanovic Djordjevic, 
and Hoa Ben The Nguyen.

## Third-Party Software
ModelFromNVE is a third party software provided by the Norwegian Energy Regulatory Authority.

